#! /usr/bin/env ruby
# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

# A little local setup:
#
#    export GITLAB_GSTG_ADMIN_API_PRIVATE_TOKEN=CHANGEME
#    export GITLAB_GPRD_ADMIN_API_PRIVATE_TOKEN=CHANGEME
#
# Staging example:
#
#    bundle exec scripts/storage_rebalance.rb nfs-file01 --staging --max-failures=1 --verbose --dry-run=yes
#
# Production examples:
#
#    bundle exec scripts/mover.rb nfs-file35 --verbose --dry-run=yes
#    bundle exec scripts/mover.rb nfs-file35 --verbose --dry-run=yes --wait=10800 --max-failures=1
#    bundle exec scripts/mover.rb nfs-file35 --verbose --dry-run=yes --count
#    bundle exec scripts/mover.rb nfs-file35 --verbose --dry-run=yes --projects=13007013
#    bundle exec scripts/mover.rb nfs-file35 --verbose --dry-run=no --move-amount=10 --skip=9271929
#    bundle exec scripts/mover.rb nfs-file53 --wait=10800 --max-failures=1 --projects=19438807 --dry-run=yes
#    bundle exec scripts/mover.rb nfs-file25 --move-amount=300 --wait=10800 --max-failures=10 --dry-run=yes
#    bundle exec scripts/mover.rb nfs-file47 --move-amount=300 --wait=10800 --max-failures=4 --dry-run=yes
#
# Migration logs may be reviewed:
#
#    export logd=./storage_migrations.d; for f in `ls -t ${logd}`; do ls -la ${logd}/$f && cat ${logd}/$f; done
#

require 'csv'
require 'date'
require 'fileutils'
require 'io/console'
require 'json'
require 'logger'
require 'net/http'
require 'optparse'
require 'time'

require_relative 'helpers/console'
require_relative 'helpers/debugging'
require_relative 'helpers/filesize'
require_relative 'helpers/logging'
require_relative 'gitlab/client'

# Storage module
module Storage
  # MoverScript module
  module MoverScript
    LIB_DIR_PATH = File.expand_path(__dir__)
    PROJECT_DIR_PATH = File.expand_path(File.dirname(LIB_DIR_PATH))
    DEFAULT_LOG_DIR_PATH = File.expand_path(File.join(PROJECT_DIR_PATH, 'storage_migrations.d'))
    LOG_META_INFO_LENGTH = 26
    DISPLAY_WIDTH = 80 - LOG_META_INFO_LENGTH
    DEFAULT_NODE_CONFIG = {}.freeze
    PROJECT_FIELDS = %i[
      id name full_path disk_path repository_storage
      size repository_size_bytes
    ].freeze
    INTEGER_PATTERN = /\A\d+\Z/.freeze
    PROGRESS_BRAILLES = [
      "\u28F7", "\u28EF", "\u28DF", "\u287F", "\u28BF", "\u28FB", "\u28FD", "\u28FE"
    ].freeze
    PROGRESS_FULL_BRAILLE = "\u28FF"
    PROGRESS_MESSAGE_FORMAT = '%<message>s%<bar>s%<spinner>s'
    SECONDS_PER_SPIN = 5
    DEFAULT_TIMEOUT = 60 * 5

    # Config module
    module Config
      DEFAULTS = {
        dry_run: true,
        log_level: Logger::INFO,
        environment: :staging,
        projects: File.join(PROJECT_DIR_PATH, 'projects.json'),
        token_env_variable_names: {
          staging: 'GITLAB_GSTG_ADMIN_API_PRIVATE_TOKEN',
          production: 'GITLAB_GPRD_ADMIN_API_PRIVATE_TOKEN'
        },
        password_prompt: 'Enter Gitlab admin API private token: ',
        console_nodes: {
          staging: 'console-01-sv-gstg.c.gitlab-staging-1.internal',
          production: 'console-01-sv-gprd.c.gitlab-production.internal'
        },
        api_endpoints: {
          staging: 'https://staging.gitlab.com/api/v4',
          production: 'https://gitlab.com/api/v4'
        },
        projects_api_uri: 'projects',
        projects_per_page: 100,
        projects_repository_api_uri: 'projects/%<project_id>s/repository',
        projects_by_id_api_uri: 'projects/%<project_id>s',
        projects_repository_storage_move_api_uri: 'projects/%<project_id>s/' \
          'repository_storage_moves/%<repository_storage_move_id>s',
        projects_repository_storage_moves_api_uri: 'projects/%<project_id>s/repository_storage_moves',
        projects_statistics_api_uri: 'projects/%<project_id>s/statistics',
        project_repository_move_states: {
          # For reference to the API state enumeration only:
          # initial: 1,
          # scheduled: 2,
          # started: 3,
          # finished: 4,
          # failed: 5,
          success: :finished,
          failure: :failed,
          in_progress: %i[initial scheduled started]
        },
        project_selector_script_path: '/var/opt/gitlab/scripts/storage_project_selector.rb',
        project_selector_command: 'sudo gitlab-rails runner ' \
          '%<project_selector_script_path>s ' \
          '%<source_shard>s %<destination_shard>s',
        source_projects: [],
        ssh_command: 'ssh %<hostname>s -- %<command>s',
        move_amount: 0,
        repository_storage_update_timeout: 10800,
        max_failures: 3,
        retry_known_failures: false,
        limit: -1,
        use_tty_display_settings: false,
        excluded_projects: [],
        log_dir_path: ::Storage::MoverScript::DEFAULT_LOG_DIR_PATH,
        migration_logfile_name: 'migrated_projects_%{date}.log',
        migration_error_logfile_prefix: 'failed_projects_',
        migration_error_logfile_name: 'failed_projects_%{date}.log',
        log_format: "%<timestamp>s %-5<level>s %<msg>s\n",
        log_format_progress: "\r%<timestamp>s %-5<level>s %<msg>s"
      }.freeze
    end
  end

  class UserError < StandardError; end
  class MigrationErrors < StandardError
    attr_reader :migration_errors

    def initialize(message, migration_errors = [])
      super(message)
      @migration_errors = migration_errors
    end
  end
  class Timeout < StandardError; end
  class NoCommits < StandardError; end
  class MigrationFailure < StandardError; end
  class ServiceFailure < StandardError
    attr_reader :repository_move

    def initialize(message, migration_state_info = nil)
      super(message)
      @repository_move = migration_state_info
    end
  end
  class CommitsMismatch < StandardError; end
  class ShardMismatch < StandardError; end
end

# Re-open the Storage module to add the Helpers module
module Storage
  # Helper methods
  module Helpers
    DEFAULT_LOG_ERROR_OPTIONS = {
      include_backtrace: false,
      persist: true
    }.freeze
    ISO8601_FRACTIONAL_SECONDS_LENGTH = 3
    include ::Helpers::Console
    include ::Helpers::Filesize

    def log_and_record_migration_error(error, project, options = {})
      options = DEFAULT_LOG_ERROR_OPTIONS.merge(options)
      if error.respond_to?(:response)
        error_body = JSON.parse(error.response.body)
        symbolize_keys_deep!(error_body)
        error_message = error_body.fetch(:message, nil)
      end
      error_message ||= error.message if error.respond_to?(:message)
      error_record = { project_id: project[:id], message: error_message }
      error_record[:disk_path] = project[:disk_path] if project.include?(:disk_path)
      log_migration_error(project, error_record) if options[:persist]
      migration_errors << error_record
      if options[:include_backtrace]
        error.backtrace.each { |t| log.error t }
      else
        log.error error
      end
      log.warn "Skipping migration"
    end

    def load_migration_failures
      logfiles_glob = File.join(options[:log_dir_path], format('%s*.log', options[:migration_error_logfile_prefix]))
      migration_failures = []
      Dir.glob(logfiles_glob).each do |file_path|
        IO.foreach(file_path) do |line|
          migration_failures << JSON.parse(line).transform_keys!(&:to_sym)
        end
      end
      migration_failures
    end

    def log_migration(project)
      log_artifact = {
        id: project[:id],
        path: project[:disk_path],
        source: project[:repository_storage],
        date: DateTime.now.iso8601(ISO8601_FRACTIONAL_SECONDS_LENGTH)
      }
      log_artifcat[destination] = options[:destination_shard] unless options[:destination_shard].nil?
      migration_log.info log_artifact.to_json
    end

    def log_migration_error(project, error)
      log_artifact = error.merge(
        source: project[:repository_storage],
        date: DateTime.now.iso8601(ISO8601_FRACTIONAL_SECONDS_LENGTH)
      )
      log_artifcat[destination] = options[:destination_shard] unless options[:destination_shard].nil?
      migration_error_log.error log_artifact.to_json
    end

    def execute_remote_command(hostname, command)
      execute_command(format(options[:ssh_command], hostname: hostname, command: command))
    end

    def symbolize_keys_deep!(memo)
      memo.keys.each do |key|
        symbolized_key = key.respond_to?(:to_sym) ? key.to_sym : key
        memo[symbolized_key] = memo.delete(key) # Preserve order even when key == symbolized_key
        symbolize_keys_deep!(memo[symbolized_key]) if memo[symbolized_key].is_a?(Hash)
      end
    end

    def set_api_token_or_else(&on_failure)
      prompt = options[:password_prompt]
      environment = options[:environment]
      token_env_variable_names = options[:token_env_variable_names]
      env_variable_name = token_env_variable_names[environment]
      token = ENV.fetch(env_variable_name, nil)
      if token.nil? || token.empty?
        log.warn "No #{env_variable_name} variable set in environment"
        token = password_prompt(prompt)
        if token.nil? || token.empty?
          raise 'Failed to get token' unless block_given?

          on_failure.call
        end
      end
      gitlab_api_client.required_headers['Private-Token'] = token
    end

    def console_node_hostname
      console_nodes = options[:console_nodes]
      environment = options[:environment]
      fqdn = console_nodes.include?(environment) ? console_nodes[environment] : console_nodes[:production]
      abort 'No console node is configured' if fqdn.nil? || fqdn.empty?

      fqdn
    end

    def get_api_url(resource_path)
      raise 'No such resource path is configured' unless options.include?(resource_path)

      endpoints = options[:api_endpoints]
      environment = options[:environment]
      endpoint = endpoints.include?(environment) ? endpoints[environment] : endpoints[:production]
      abort 'No api endpoint url is configured' if endpoint.nil? || endpoint.empty?

      [endpoint, options[resource_path]].join('/')
    end

    def all_specify?(projects, key)
      return false if projects.empty? || !projects.respond_to?(:all?)

      projects.all? { |project| project.include?(key) }
    end

    def get_columns
      if options[:use_tty_display_settings]
        _rows, columns = IO.console.winsize
        columns -= ::Storage::MoverScript::LOG_META_INFO_LENGTH
      else
        columns = ::Storage::MoverScript::DISPLAY_WIDTH
      end
      columns
    end

    def progress_message(message, bar, spinner)
      format(::Storage::MoverScript::PROGRESS_MESSAGE_FORMAT, message: message, bar: bar, spinner: spinner)
    end

    def loop_with_progress_until(timeout = ::Storage::MoverScript::DEFAULT_TIMEOUT, message = '', &block)
      progress_character = ::Storage::MoverScript::PROGRESS_FULL_BRAILLE
      spinner_characters = ::Storage::MoverScript::PROGRESS_BRAILLES
      seconds_per_spin = ::Storage::MoverScript::SECONDS_PER_SPIN
      spinner_pause_interval_seconds = 1 / spinner_characters.length.to_f
      columns = get_columns
      bar_characters = ''
      iteration = 0
      start = Time.now.to_i
      loop do
        break if block_given? && block.call == true

        elapsed = Time.now.to_i - start
        raise Timeout, "Timed out after #{elapsed} seconds" if elapsed >= timeout

        seconds_per_spin.times do
          spinner_characters.each do |character|
            progress_log.info(progress_message(message, bar_characters, character))
            sleep(spinner_pause_interval_seconds)
          end
        end
        iteration += 1
        bar_characters = progress_character * (iteration % columns)
        progress_log.info(progress_message(message, bar_characters, progress_character))
        if (iteration % columns).zero?
          bar_characters = ''
          $stdout.write("\n")
        elsif log.level == Logger::DEBUG
          $stdout.write("\n")
        end
        $stdout.flush
      end
    ensure
      puts "\n"
    end
  end
end

# Re-open the Storage module to add the CommandLineSupport module
module Storage
  # Support for command line arguments
  module CommandLineSupport
    # OptionsParser class
    class OptionsParser
      include ::Storage::Helpers
      attr_reader :parser, :options

      def initialize
        @parser = OptionParser.new
        @options = ::Storage::MoverScript::Config::DEFAULTS.dup
        define_options
      end

      def define_options
        @parser.banner = "Usage: #{$PROGRAM_NAME} [options]"
        @parser.separator ''
        @parser.separator 'Options:'
        define_head
        define_dry_run_option
        define_projects_option
        define_project_ids_option
        define_csv_projects_option
        define_move_amount_option
        define_skip_option
        define_per_page_option
        define_wait_option
        define_max_failures_option
        define_retry_known_failures_option
        define_limit_option
        define_tty_option
        define_env_option
        define_verbose_option
        define_tail
      end

      def define_head
        description = 'Name of the source gitaly storage shard server'
        @parser.on_head('<source_shard>', description) do |server|
          @options[:source_shard] = server
        end
        description = 'Name of the destination gitaly storage shard server'
        @parser.on_head('<destination_shard>', description) do |server|
          @options[:destination_shard] = server
        end
      end

      def parse_dry_run_argument(arg)
        case arg
        when /^(no|false)$/i then false
        when /^(yes|true)$/i then true
        else raise OptionParser::InvalidArgument
        end
      end

      def define_dry_run_option
        description = "Show what would have been done; default: yes"
        @parser.on('-d', "--dry-run=<yes|no>", description) do |dry_run|
          @options[:dry_run] = parse_dry_run_argument(dry_run)
        end
      end

      def define_project_ids_option
        description = 'Select specific projects to migrate'
        @parser.on('--project-ids=<project_id,...>', Array, description) do |project_ids|
          @options[:source_projects] ||= []
          unless project_ids.respond_to?(:all?) && project_ids.all? { |s| resembles_integer? s }
            message = 'Argument given for --project-ids must be a list of one or more integers'
            raise OptionParser::InvalidArgument, message
          end

          projects.each do |project|
            if (project_id = project.to_i).positive?
              @options[:source_projects].push({ id: project_id })
            end
          end
        end
      end

      def read_file(file_path)
        data = {}
        begin
          data = IO.read(file_path)
          data = JSON.parse(data) unless data.nil? || data.empty?
        rescue StandardError => e
          log.error 'Error loading projects: ' + e.message
          e.backtrace.each { |t| log.error t }
        end
        data
      end

      def load_projects(file_path = @options[:projects])
        read_file(file_path).fetch('projects', []).map do |project|
          project.transform_keys!(&:to_sym)
        end
      end

      def define_projects_option
        description = "Absolute path to JSON file enumerating projects ids; default: #{@options[:projects]}"
        @parser.on('--projects=<file_path>', description) do |file_path|
          unless File.exist?(file_path)
            message = 'Argument given for --projects must be a path to an existing file'
            raise OptionParser::InvalidArgument, message
          end

          @options[:source_projects] ||= []
          @options[:source_projects].concat(load_projects(file_path))
        end
      end

      def define_csv_projects_option
        description = 'Absolute path to CSV file enumerating projects ids'
        @parser.on('--csv-projects=<file_path>', description) do |file_path|
          unless File.exist? file_path
            message = 'Argument given for --csv must be an absolute path to an existing file'
            raise OptionParser::InvalidArgument, message
          end

          # TODO: Refactor to a helper method
          @options[:source_projects] ||= []
          projects = CSV.new(file_path).to_a
          projects.collect do |project_values|
            project = {}
            project_values.each_with_index do |value, i|
              project[::Storage::MoverScript::PROJECT_FIELDS[i]] = value
            end
            project
          end
          @options[:source_projects].concat(projects)
        end
      end

      def define_move_amount_option
        description = "Gigabytes of repo data to move; default: #{@options[:move_amount]}, or " \
          'largest single repo if 0'
        @parser.on('-m', '--move-amount=<n>', Integer, description) do |move_amount|
          abort 'Size too large' if move_amount > 16_000
          # Convert given gigabytes to bytes
          @options[:move_amount] = (move_amount * 1024 * 1024 * 1024)
        end
      end

      def define_skip_option
        description = 'Skip specific project(s)'
        @parser.on('--skip=<project_id,...>', Array, description) do |project_identifiers|
          @options[:excluded_projects] ||= Set.new
          if project_identifiers.respond_to?(:all?) &&
              project_identifiers.all? { |s| resembles_integer? s }
            positive_numbers = project_identifiers.map(&:to_i).delete_if { |i| !i.positive? }
            @options[:excluded_projects] |= positive_numbers.uniq
          else
            message = 'Argument given for --skip must be a list of one or more integers'
            raise OptionParser::InvalidArgument, message
          end
        end
      end

      def define_per_page_option
        description = "Projects per page to request; default: #{@options[:projects_per_page]}"
        @parser.on('--per-page=<n>', Integer, description) do |per_page|
          abort 'Given projects per-page must be between 1 and 100' if per_page < 1 || per_page > 100
          @options[:projects_per_page] = per_page
        end
      end

      def define_wait_option
        description = "Timeout in seconds for migration completion; default: " \
          "#{@options[:repository_storage_update_timeout]}"
        @parser.on('-w', '--wait=<n>', Integer, description) do |wait|
          @options[:repository_storage_update_timeout] = wait
        end
      end

      def define_max_failures_option
        description = "Maximum failed migrations; default: #{@options[:max_failures]}"
        @parser.on('-f', '--max-failures=<n>', Integer, description) do |failures|
          @options[:max_failures] = failures
        end
      end

      def define_retry_known_failures_option
        description = "Retry known recorded migration failures; default: #{@options[:retry_known_failures]}"
        @parser.on('-f', '--retry-known-failures', description) do
          @options[:max_failures] = true
        end
      end

      def define_limit_option
        description = "Maximum migrations; default: #{@options[:limit]}"
        @parser.on('-l', '--limit=<n>', Integer, description) do |limit|
          @options[:limit] = limit
        end
      end

      def define_include_mirrors_option
        @parser.on('-M', '--include-mirrors', 'Include mirror repositories') do |include_mirrors|
          @options[:include_mirrors] = true
        end
      end

      def define_tty_option
        @parser.on('--tty', 'Format progress display using tty settings') do
          @options[:use_tty_display_settings] = true
        end
      end

      def validated_env(env)
        env = env.to_sym
        raise OptionParser::InvalidArgument, "Invalid environment: #{env}" unless @options[:api_endpoints].key?(env)

        env
      end

      def define_env_option
        description = "Operational environment; default: #{@options[:environment]}"
        @parser.on('--environment=<staging|production>', description) do |env|
          @options[:environment] = validated_env(env)
        end
      end

      def define_verbose_option
        @parser.on('-v', '--verbose', 'Increase logging verbosity') do
          @options[:log_level] -= 1
        end
      end

      def define_tail
        @parser.on_tail('-?', '--help', 'Show this message') do
          puts @parser
          exit
        end
      end

      def resembles_integer?(obj)
        ::Storage::MoverScript::INTEGER_PATTERN.match?(obj.to_s)
      end
    end
    # class OptionsParser

    def demand(options, arg, type = :positional)
      return options[arg] unless options[arg].nil?

      required_arg = "<#{arg}>"
      required_arg = "--#{arg.to_s.gsub(/_/, '-')}" if type == :optional
      raise UserError, "Required argument: #{required_arg}"
    end

    def parse(args = ARGV, file_path = ARGF)
      opt = OptionsParser.new
      args.push('-?') if args.empty?
      opt.parser.parse!(args)
      opt.options[:source_shard] = args.shift
      opt.options[:destination_shard] = args.shift
      # TODO: Handle with a helper method
      unless STDIN.tty? || STDIN.closed?
        projects = CSV.new(file_path).to_a
        projects.each { |project| opt.options[:source_projects].push(Project.new(*project)) }
      end
      opt.options
    rescue OptionParser::InvalidArgument, OptionParser::InvalidOption,
           OptionParser::MissingArgument, OptionParser::NeedlessArgument => e
      puts e.message
      puts opt.parser
      exit 1
    rescue OptionParser::AmbiguousOption => e
      abort e.message
    end
  end
  # module CommandLineSupport
end
# module Storage

# Re-open the Storage module to define the Mover class
module Storage
  # The Mover class
  class Mover
    include ::Helpers::Debugging
    include ::Helpers::Logging
    include ::Storage::Helpers
    attr_reader :options, :gitlab_api_client, :migration_errors

    def initialize(options)
      @options = options
      @gitlab_api_client = ::GitLab::Client.new(@options)
      @migration_errors = []
      log.level = @options[:log_level]
      @pagination_indices = {}
    end

    def notify_slack(message)
      return unless ENV['SLACK_WEBHOOK_URL'] && ENV['SLACK_NOTIFY']

      ci_job_link = ":ci_running: This message was sent from this <#{ENV['CI_JOB_URL']}|CI job>"

      if options[:dry_run]
        log.info("SLACK NOTIFICATION DRY RUN: This message would have been sent to Slack")
        log.info("URL: #{ENV['SLACK_WEBHOOK_URL']}")
        log.info("Channel: #{ENV['SLACK_CHANNEL']}")
        log.info([message, ci_job_link].join("\n"))
        return
      end

      uri     = URI(ENV['SLACK_WEBHOOK_URL'])
      request = Net::HTTP::Post.new(uri)

      payload = {
        blocks: [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: message
            }
          },
          {
            type: 'context',
            elements: [
              {
                type: 'mrkdwn',
                text: ci_job_link
              }
            ]
          }
        ],
        channel: ENV['SLACK_CHANNEL']
      }
      request.set_form_data('payload' => JSON.generate(payload))

      Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
        response = http.request(request)
        response.body
      end
    end

    def fetch_project(project_id)
      return {} if project_id.nil? || project_id.to_s.empty?

      url = format(get_api_url(:projects_by_id_api_uri), project_id: project_id)
      project, error, status, _headers = gitlab_api_client.get(
        url, parameters: { statistics: true })
      raise error unless error.nil?

      raise "Invalid response status code: #{status}" unless [200].include?(status)

      raise "Failed to get project id: #{project_id}" if project.nil? || project.empty?

      symbolize_keys_deep!(project)
      project
    # rescue ::GitLab::Client::NotFound => e
    #   log.error "Not found error fetching project id: #{project_id}: " + e.message
    #   raise e
    # rescue ::GitLab::Client::Unauthorized => e
    #   log.error "Authorization error fetching project id: #{project_id}: " + e.message
    #   raise e
    # rescue ::GitLab::Client::InternalServerError => e
    #   log.error "Internal server error fetching project id: #{project_id}: " + e.message
    #   raise e
    # rescue Net::HTTPFatalError => e
    #   log.error "Unexpected server error fetching project id: #{project_id}"
    #   raise e
    # rescue Net::HTTPClientException => e
    #   log.error "Unexpected client error fetching project id: #{project_id}"
    #   raise e
    rescue StandardError => e
      log.error "Unexpected error fetching project id: #{project_id}: #{e} (#{e.class})"
      raise e
    end

    # Execute remote script to fetch largest projects
    def fetch_largest_projects(next_page = false)
      source_shard = options[:source_shard]
      url = get_api_url(:projects_api_uri)
      parameters = {
        order_by: 'repository_size',
        statistics: true,
        repository_storage: source_shard,
        per_page: options[:projects_per_page]
      }
      parameters['page'] = @pagination_indices[__method__] if @pagination_indices.include?(__method__)
      projects, error, status, headers = gitlab_api_client.get(url, parameters: parameters)
      raise error unless error.nil?

      @pagination_indices[__method__] = headers['x-next-page'].first

      raise "Invalid response status code: #{status}" unless [200].include?(status)

      raise "Unexpected response: #{projects}" if projects.nil? || projects.empty?

      projects.each { |project| symbolize_keys_deep!(project) }
      projects
    end

    def fetch_repository_storage_move(project, repository_storage_move)
      url = format(
        get_api_url(:projects_repository_storage_move_api_uri),
        project_id: project[:id],
        repository_storage_move_id: repository_storage_move[:id])
      move, error, status, _headers = gitlab_api_client.get(url)
      raise error unless error.nil?

      raise "Invalid response status code: #{status}" unless [200].include?(status)

      raise "Unexpected response: #{move}" unless move.fetch('project', {}).fetch('id', nil) == project[:id]

      symbolize_keys_deep!(move)
      move
    end

    def fetch_repository_storage_moves(project)
      url = format(get_api_url(:projects_repository_storage_moves_api_uri), project_id: project[:id])
      moves, error, status, _headers = gitlab_api_client.get(url)
      raise error unless error.nil?

      raise "Invalid response status code: #{status}" unless [200].include?(status)

      raise "Unexpected response: #{moves}" if moves.nil?

      moves.each { |element| symbolize_keys_deep!(element) }
      moves
    end

    def create_repository_storage_move(project, destination)
      url = format(get_api_url(:projects_repository_storage_moves_api_uri), project_id: project[:id])

      body = {}
      body[:destination_storage_name] = destination unless destination.nil?

      move, error, status, _headers = gitlab_api_client.post(url, body: body)
      raise error unless error.nil?

      raise "Invalid response status code: #{status}" unless [200, 201].include?(status)

      raise "Unexpected response: #{move}" unless move.fetch('project', {}).fetch('id', nil) == project[:id]

      symbolize_keys_deep!(move)
      move
    end

    def wait_for_repository_storage_move(project, repository_storage_move, opts = {})
      project_repository_move_states = options[:project_repository_move_states].merge(opts)
      timeout = options[:repository_storage_update_timeout]
      success_state = project_repository_move_states[:success]
      failure_state = project_repository_move_states[:failure]
      message = "Moving project id: #{project[:id]}: "

      begin
        loop_with_progress_until(timeout, message) do
          with_log_level(Logger::INFO) do
            repository_move = fetch_repository_storage_move(project, repository_storage_move)
            repository_move_state = repository_move.fetch(:state, nil)&.to_sym
            repository_destination_shard = repository_move.fetch(:destination_storage_name, nil)
            if repository_move_state == failure_state
              message = 'Noticed service failure during repository replication'
              message += " to #{repository_destination_shard}" unless repository_destination_shard.nil?
              raise ServiceFailure.new(message, repository_move)
            end
            repository_move_state == success_state
          end
        end
      rescue Timeout => e
        log.warn "Gave up waiting for repository storage move to reach state: #{success_state}: #{e.message}"
      end
    end

    def verify_source(project)
      return if options[:source_shard].nil? || project[:repository_storage] == options[:source_shard]

      raise ShardMismatch, "Repository for project id: #{project[:id]} is on shard: " \
        "#{project[:repository_storage]} not #{options[:source_shard]}"
    end

    def summarize(total)
      log_separator
      log.info "Done"
      if total&.positive?
        message = if options[:dry_run]
                    "[Dry-run] Would have processed #{to_filesize(total)} of data"
                  else
                    "Rebalancing done. Processed #{to_filesize(total)} of data"
                  end

        log.info(message)
        notify_slack(message)
      end
      return if options[:dry_run]

      log.info "Finished migrating projects"
      raise MigrationErrors.new('Encountered migration errors', migration_errors) unless migration_errors.empty?

      log.info "No errors encountered during migration"
    end

    def repository_replication_already_in_progress?(project)
      log.debug "Checking for existing repository replications for project id #{project[:id]}"
      fetch_repository_storage_moves(project).any? do |move|
        options[:project_repository_move_states][:in_progress].include?(move[:state]&.to_sym)
      end
    end

    def verify_migration(project, source, destination)
      post_migration_shard = project[:repository_storage]
      if !destination.nil? && post_migration_shard != destination
        raise MigrationFailure, "Project id: #{project[:id]} resides on: #{post_migration_shard} " \
          "(expected: #{destination})"
      elsif post_migration_shard == source
        raise MigrationFailure, "Project id: #{project[:id]} remains on source shard: #{post_migration_shard}"
      end

      log.info "Success moving project id: #{project[:id]} to #{post_migration_shard}"
    end

    # rubocop: disable Metrics/AbcSize
    def schedule_repository_move(project)
      verify_source(project)
      if repository_replication_already_in_progress?(project)
        log.warn "Repository replication for project id #{project[:id]} already in progress; skipping"
        return
      end

      source = project[:repository_storage]
      destination = options[:destination_shard] || project[:destination_repository_storage]
      log_separator
      log.info "Scheduling repository move for project id: #{project[:id]}"
      log.info "  Project path: #{project[:path_with_namespace]}"
      log.info "  Current shard name: #{project[:repository_storage]}"
      log.info "  Disk path: #{project[:disk_path]}" if project.include?(:disk_path)
      if project.include?(:statistics)
        log.info "  Repository size: #{to_filesize(project[:statistics][:repository_size])}"
      elsif project.include?(:size)
        log.info "  Repository size: #{project[:size]}"
      end

      if options[:dry_run]
        log.info "[Dry-run] Would have scheduled repository move for project id: #{project[:id]}"
        return
      end

      repository_storage_move = create_repository_storage_move(project, destination)
      wait_for_repository_storage_move(project, repository_storage_move)
      post_migration_project = project.merge(fetch_project(project[:id]))
      verify_migration(post_migration_project, source, destination)

      log.info "Migrated project id: #{post_migration_project[:id]}"
      log.debug "  Project path: #{post_migration_project[:path_with_namespace]}"
      log.debug "  Current shard name: #{post_migration_project[:repository_storage]}"
      log.debug "  Disk path: #{post_migration_project[:disk_path]}" if post_migration_project.include?(:disk_path)
      log_migration(project)
    # rescue ::GitLab::Client::NotFound => e
    #   log.error "Not found error moving project id: #{project[:id]}: " + e.message
    #   log_and_record_migration_error(e, project)
    # rescue ::GitLab::Client::Unauthorized => e
    #   log.error "Authorization error moving project id: #{project[:id]}: " + e.message
    #   log_and_record_migration_error(e, project)
    # rescue ::GitLab::Client::InternalServerError => e
    #   log.error "Internal server error moving project id: #{project[:id]}: " + e.message
    #   log_and_record_migration_error(e, project)
    # rescue Net::HTTPFatalError => e
    #   log.error "Unexpected server error moving project id: #{project[:id]}"
    #   log_and_record_migration_error(e, project)
    # rescue Net::HTTPClientException => e
    #   log.error "Unexpected client error moving project id: #{project[:id]}"
    #   log_and_record_migration_error(e, project)
    rescue StandardError => e
      log.error "Unexpected error moving project id: #{project[:id]}: #{e} (#{e.class})"
      log_and_record_migration_error(e, project, include_backtrace: true, persist: false)
    end
    # rubocop: enable Metrics/AbcSize

    def move_project(project)
      project_id = project[:id]
      project_info = fetch_project(project_id)
      project.update(project_info)

      schedule_repository_move(project)

      post_move_project_info = fetch_project(project_id)
      get_repository_size(post_move_project_info)
    end

    def get_repository_size(project)
      size = project.fetch(:statistics, {}).fetch(:repository_size, nil)
      raise "Absent field 'statistics.repository_size' for project id: #{project[:id]}" if size.nil?

      size
    end

    def exclude_known_failures
      failed_reprojectories = load_migration_failures.collect { |failure| failure.fetch(:project_id, nil) }
      failed_reprojectories.compact!
      log.info "Filtering #{failed_reprojectories.length} known failed project repositories"
      log.debug "Known failed project repositories: #{failed_reprojectories}"
      options[:excluded_projects] |= failed_reprojectories
    end

    def get_projects
      projects = @options[:source_projects].map { |project| fetch_project(project[:id]) }
      @options[:source_projects].clear
      if projects.empty?
        exclude_known_failures unless options[:retry_known_failures]
        next_page = false
        # This loop is only here to ensure that if projects are excluded
        # (typicaly because of previous known replication failures) from a
        # returned page of projects, that a subsequent page is requested in
        # the case that the current page is full of nothing but projects which
        # are presumed to be doomed to fail their replication operation.
        loop do
          log.info format('Fetching %slargest projects', next_page ? 'next page of ' : '')
          projects = fetch_largest_projects(next_page)
          # When there are no more projects in the API result set:
          break if projects.empty?

          projects.reject! { |project| options[:excluded_projects].include?(project[:id]) }
          # When there are non-failed projects in the API result set:
          break unless projects.empty?

          # When there are no non-failed projects in the API result set,
          # get the next page.
          next_page = true
        end
      end
      projects = projects[0...options[:limit]] if options[:limit].positive?
      projects
    end

    def paginate_projects(&block)
      initial_projects = @options[:source_projects]
      projects = initial_projects.dup
      loop do
        projects = get_projects if projects.empty? && initial_projects.empty?
        break if projects.nil? || projects.empty?

        return projects unless block_given?

        project = projects.shift
        break if project.nil?

        next if project.empty?

        yield project
      end
    end

    def move_projects(min_amount = options[:move_amount], limit = options[:limit])
      log.debug "Project migration validation timeout: " \
        "#{options[:repository_storage_update_timeout]} seconds"

      moved_projects_count = 0
      total_bytes_moved = 0

      paginate_projects do |project|
        repository_size_bytes = move_project(project)
        if migration_errors.length >= options[:max_failures]
          log.error "Failed too many times"
          break
        end
        moved_projects_count += 1
        total_bytes_moved += repository_size_bytes
        break if limit.positive? && moved_projects_count >= limit
        break if min_amount.positive? && total_bytes_moved > min_amount
      end
      total_bytes_moved
    end

    def move
      migration_log
      limit = options[:limit]
      move_amount_bytes = options[:move_amount]
      if limit.positive? && move_amount_bytes.positive?
        message = "Balancer will move #{limit} projects or #{to_filesize(move_amount_bytes)} worth of " \
           "data, whichever is reached first."
        notify_slack(":warning: Gitaly balancer is starting rebalance. " + message)
        log.info(message)
      elsif limit.positive?
        message = "Balancer will move #{limit} projects."
        notify_slack(":warning: Gitaly balancer is starting rebalance. " + message)
        log.info(message)
      elsif move_amount_bytes.positive?
        message = "Balancer will move at least #{to_filesize(move_amount_bytes)} worth of data."
        notify_slack(":warning: Gitaly balancer is starting rebalance. " + message)
        log.info(message)
      elsif @options[:source_projects].empty?
        options[:limit] = 1
        log.info 'User has specified neither --limit nor --move-amount and has given no projects'
        message = 'Balancer will only move 1 fetched project.'
        notify_slack(":warning: Gitaly balancer is starting rebalance. " + message)
        log.info(message)
      else
        log.info 'User has specified neither --limit nor --move-amount'
        log.info 'Balancer will move all given projects'
        notify_slack(":warning: Gitaly balancer is starting rebalance.")
      end

      total_bytes_moved = move_projects
      summarize(total_bytes_moved)
      nil # Signifies no error
    end
  end
  # class Mover
end
# module Storage

# Re-open the Storage module to add MoverScript module
module Storage
  # MoverScript module
  module MoverScript
    include ::Helpers::Debugging
    include ::Helpers::Logging
    include ::Storage::Helpers
    include ::Storage::CommandLineSupport

    def main(args = parse(ARGV, ARGF))
      dry_run_notice if args[:dry_run]
      mover = ::Storage::Mover.new(args)
      mover.set_api_token_or_else do
        raise UserError, 'Cannot proceed without a GitLab admin API private token'
      end
      mover.move
    rescue MigrationErrors => e
      log.error "Encountered #{e.migration_errors.length} errors:"
      log.error JSON.pretty_generate(e.migration_errors)
      log.error 'See storage_migrations.d/failed_projects_*.log for more details'
      exit 1
    rescue UserError => e
      abort e.message
    rescue StandardError => e
      log.fatal e.message
      e.backtrace.each { |trace| log.error trace }
      abort
    rescue SystemExit
      exit
    rescue Interrupt => e
      $stdout.write "\r\n#{e.class}\n"
      $stdout.flush
      $stdin.echo = true if $stdin.respond_to?(:echo=)
      exit 0
    end
  end
  # MoverScript module
end
# Storage module

# Anonymous object avoids namespace pollution
Object.new.extend(::Storage::MoverScript).main if $PROGRAM_NAME == __FILE__
