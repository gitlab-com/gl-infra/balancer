#! /usr/bin/env ruby
# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

require 'erubis'

require 'fileutils'
require 'io/console'
require 'json'
require 'optparse'

require_relative 'helpers/logging'
require_relative 'thanos/client'

# Storage module
module Storage
  # ShardSelectorScript module
  module ShardSelectorScript
    LIB_DIR_PATH = File.expand_path(__dir__)
    PROJECT_DIR_PATH = File.expand_path(File.dirname(LIB_DIR_PATH))
    THANOS_QUERIES_DIR_PATH = File.expand_path(File.join(PROJECT_DIR_PATH, 'thanos_queries.d'))
    SIMULATED_SHARDS = %w[nfs-file01 nfs-file09].freeze

    # Config module
    module Config
      DEFAULTS = {
        log_level: Logger::INFO,
        environment: :staging,
        shards_selection_thanos_queries: {
          disk_usage_average: File.join(
            THANOS_QUERIES_DIR_PATH, 'node_filesystem_avail_ratio_average.promql.erb'),
          combined: File.join(
            THANOS_QUERIES_DIR_PATH, 'shard_combined_selection.promql.erb'),
          disk_usage: File.join(
            THANOS_QUERIES_DIR_PATH, 'node_filesystem_usage.promql.erb'),
          disk_available: File.join(
            THANOS_QUERIES_DIR_PATH, 'node_filesystem_avail.promql.erb'),
          bytes_written: File.join(
            THANOS_QUERIES_DIR_PATH, 'node_disk_written_bytes_total.promql.erb'),
          bytes_read: File.join(
            THANOS_QUERIES_DIR_PATH, 'node_disk_read_bytes_total.promql.erb'),
          writes: File.join(
            THANOS_QUERIES_DIR_PATH, 'node_disk_writes_completed_total.promql.erb'),
          reads: File.join(
            THANOS_QUERIES_DIR_PATH, 'node_disk_reads_completed_total.promql.erb'),
          client_starts: File.join(
            THANOS_QUERIES_DIR_PATH, 'grpc_client_started_total.promql.erb')
        },
        criteria: :disk_usage,
        output: File.join(PROJECT_DIR_PATH, "#{File.basename($PROGRAM_NAME, '.*')}.json"),
        api_endpoints: {
          development: 'http://localhost:10902/api/v1',
          staging: 'http://thanos-query-frontend-internal.ops.gke.gitlab.net:9090/api/v1',
          production: 'http://thanos-query-frontend-internal.ops.gke.gitlab.net:9090/api/v1'
        },
        prometheus_environment_identifiers: {
          development: 'gprd',
          staging: 'gstg',
          production: 'gprd'
        },
        query_api_uri: 'query',
        excluded: [],
        limit: -1,
        simulate: false,
        simulated_shards: SIMULATED_SHARDS,
        threshold: 0.147
      }.freeze
    end
  end
  # module ShardSelectorScript

  class UserError < StandardError; end
  class Timeout < StandardError; end
end

# Re-open the Storage module to add the Helpers module
module Storage
  # Helper methods
  module Helpers
    SHARD_APP_NAME_TEMPLATE = 'nfs-%<shard>s'

    def get_api_url(resource_path)
      raise 'No such resource path is configured' unless options.include?(resource_path)

      endpoints = options[:api_endpoints]
      environment = options[:environment]
      endpoint = endpoints.include?(environment) ? endpoints[environment] : endpoints[:production]
      abort 'No api endpoint url is configured' if endpoint.nil? || endpoint.empty?

      [endpoint, options[resource_path]].join('/')
    end

    def symbolize_keys_deep!(memo)
      memo.keys.each do |key|
        symbolized_key = key.respond_to?(:to_sym) ? key.to_sym : key
        memo[symbolized_key] = memo.delete(key) # Preserve order even when key == symbolized_key
        symbolize_keys_deep!(memo[symbolized_key]) if memo[symbolized_key].is_a?(Hash)
      end
    end
  end
end

# Re-open the Storage module to add the CommandLineSupport module
module Storage
  # Support for command line arguments
  module CommandLineSupport
    # OptionsParser class
    class OptionsParser
      include ::Storage::Helpers
      attr_reader :parser, :options

      def initialize
        @parser = OptionParser.new
        @options = ::Storage::ShardSelectorScript::Config::DEFAULTS.dup
        define_options
      end

      def define_options
        @parser.banner = "Usage: #{$PROGRAM_NAME} [options]"
        @parser.separator ''
        @parser.separator 'Options:'
        define_thanos_option
        define_criteria_option
        define_output_option
        define_limit_option
        define_excluded_option
        define_simulate_option
        define_env_option
        define_verbose_option
        define_tail
      end

      def define_thanos_option
        environment = @options[:environment]
        api_endpoints = @options[:api_endpoints]
        thanos_endpoint = api_endpoints[environment]
        description = "Thanos endpoint; default: #{thanos_endpoint}"
        @parser.on('-T', '--thanos=<thanos_endpont>', description) do |thanos_endpont|
          @options[:api_endpoints][environment] = thanos_endpont
        end
      end

      def validated_criteria(criteria, memo = @options[:shards_selection_thanos_queries])
        criteria = criteria.to_sym
        raise "Invalid selection criteria: #{criteria}" unless memo.key?(criteria)

        criteria
      end

      def define_criteria_option
        criterias = @options[:shards_selection_thanos_queries].keys.join('|')
        description = "Selection criteria; default: #{@options[:criteria]}"
        @parser.on(format('--criteria=<%<criterias>s>', criterias: criterias), description) do |criteria|
          @options[:criteria] = validated_criteria(criteria)
        end
      end

      def define_output_option
        description = 'Path at which to export shards or - for stdout; default: ' + @options[:output]
        @parser.on('--output=<fully_qualified_file_path|->', description) do |output|
          file_path = File.expand_path(output)
          dir_path = File.dirname(file_path)
          raise 'Argument --output path is invalid: ' + file_path unless output == '-' || File.exist?(dir_path)

          @options[:output] = file_path
        end
      end

      def define_limit_option
        description = "Selection limit; default: #{@options[:limit].negative? ? 'no limit' : @options[:limit]}"
        @parser.on('-l', '--limit=<n>', Integer, description) do |limit|
          @options[:limit] = limit
        end
      end

      def define_excluded_option
        description = "Shard exclusions; default: #{@options[:excluded].empty? ? 'none' : @options[:excluded]}"
        @parser.on('-x', '--excluded=<shard>,...', Array, description) do |excluded|
          @options[:excluded] = excluded
        end
      end

      def define_simulate_option
        description = 'Simulate shard selection; default: no'
        @parser.on('-S', '--simulate=[yes/no]', description) do |simulate|
          @options[:simulate] = simulate.match?(/^(yes|true)$/i)
        end
      end

      def validated_env(env)
        env = env.to_sym
        raise "Invalid environment: #{env}" unless @options[:api_endpoints].key?(env)

        env
      end

      def define_env_option
        environments = @options[:api_endpoints].keys.join('|')
        description = "Operational environment; default: #{@options[:environment]}"
        @parser.on(format('--environment=<%<environments>s>', environments: environments), description) do |env|
          @options[:environment] = validated_env(env)
        end
      end

      def define_verbose_option
        @parser.on('-v', '--verbose', 'Increase logging verbosity') do
          @options[:log_level] -= 1
        end
      end

      def define_tail
        @parser.on_tail('-?', '--help', 'Show this message') do
          puts @parser
          exit
        end
      end
    end
    # class OptionsParser

    def demand(options, arg, type = :positional)
      return options[arg] unless options[arg].nil?

      required_arg = "<#{arg}>"
      required_arg = "--#{arg.to_s.gsub(/_/, '-')}" if type == :optional
      raise UserError, "Required argument: #{required_arg}"
    end

    def parse(args = ARGV, file_path = ARGF)
      opt = OptionsParser.new
      opt.parser.parse!(args)
      opt.options
    rescue RuntimeError, OptionParser::InvalidArgument, OptionParser::InvalidOption,
           OptionParser::MissingArgument, OptionParser::NeedlessArgument => e
      puts e.message
      puts opt.parser
      exit 1
    rescue OptionParser::AmbiguousOption => e
      abort e.message
    end
  end
  # module CommandLineSupport
end
# module Storage

# Re-open the Storage module to define the ShardSelector class
module Storage
  # The ShardSelector class
  class ShardSelector
    include ::Helpers::Logging
    include ::Storage::Helpers
    attr_reader :options, :thanos_api_client

    def initialize(options)
      @options = options
      log.level = @options[:log_level]
      @pagination_indices = {}
      @thanos_api_client = ::Thanos::Client.new(@options)
    end

    def projection(memos, fields)
      memos.map { |memo| memo.slice(*fields) }
    end

    def load_query(file_path)
      Erubis::Eruby.new(IO.read(file_path))
    end

    def interpolate(template, args = {})
      context = Erubis::Context.new
      args.each { |key, value| context[key] = value }
      template.evaluate(context)
    end

    def fetch_shard_load_ratios
      url = get_api_url(:query_api_uri)
      env = options.fetch(:prometheus_environment_identifiers, {}).fetch(options[:environment])
      template_file_path = options.fetch(:shards_selection_thanos_queries, {}).fetch(options[:criteria])
      template = load_query(template_file_path)
      query = interpolate(template, env: env)

      parameters = {
        query: 'sort_desc(' + query + ')',
        time: Time.now.utc.to_f
      }
      log.debug('Querying Thanos for overloaded shards')
      results, error, status, _headers = thanos_api_client.get(url, parameters: parameters)
      raise error unless error.nil?

      raise "Invalid response status code: #{status}" unless [200].include?(status)

      raise "Unexpected response: #{results}" if results.nil? || results.empty?

      results.fetch('data', {}).fetch('result', [])
    end

    def to_app_shard_name(shard)
      return shard.gsub(/.*(\d+)$/, 'praefect-file\1') if shard.include?('praefect')

      format(SHARD_APP_NAME_TEMPLATE, shard: shard.gsub(/-(\d+)$/, '\1'))
    end

    def overloaded_shards
      threshold = options[:threshold]
      shard_load_ratios = fetch_shard_load_ratios
      shard_load_ratios.each_with_object([]) do |item, memo|
        shard = item.fetch('metric', {}).fetch('fqdn', '').gsub(/-stor-g.*$/, '')
        ratio = item.fetch('value', []).last.to_f
        memo << to_app_shard_name(shard) unless ratio < threshold
      end
    end

    def get_shards
      log.debug format('Fetching overloaded shards')
      return options[:simulated_shards] if options[:simulate]

      excluded = options[:excluded]
      shards = overloaded_shards
      shards.reject! { |shard| excluded.include?(shard) }
      shards = shards[0...options[:limit]] if options[:limit].positive?
      shards
    end
  end
  # module ShardSelector
end
# module Storage

# Re-open the Storage module to add ShardSelectorScript module
module Storage
  # ShardSelectorScript module
  module ShardSelectorScript
    include ::Helpers::Logging
    include ::Storage::Helpers
    include ::Storage::CommandLineSupport

    def main(args = parse(ARGV, ARGF))
      selector = ::Storage::ShardSelector.new(args)
      shards = selector.get_shards
      abort "No shards selected" if shards.empty?

      json = JSON.pretty_generate(shards: shards)
      if args[:output] == '-'
        $stdout.write(json)
      else
        File.open(args[:output], 'w') { |f| f.write(json) }
      end
    rescue UserError => e
      abort e.message
    rescue StandardError => e
      log.error e.message
      e.backtrace.each { |trace| log.error trace }
      abort
    rescue SystemExit
      exit
    rescue Interrupt => e
      $stderr.write "\r\n#{e.class}\n"
      $stderr.flush
      $stdin.echo = true if $stdin.respond_to?(:echo=)
      exit 0
    end
  end
  # ShardSelectorScript module
end
# Storage module

# Anonymous object avoids namespace pollution
Object.new.extend(::Storage::ShardSelectorScript).main if $PROGRAM_NAME == __FILE__
