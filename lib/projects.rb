#! /usr/bin/env ruby
# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

require 'fileutils'
require 'io/console'
require 'json'
require 'optparse'
require 'time'

require_relative 'helpers/console'
require_relative 'helpers/filesize'
require_relative 'helpers/logging'
require_relative 'gitlab/client'

# Storage module
module Storage
  # ProjectSelectorScript module
  module ProjectSelectorScript
    LIB_DIR_PATH = File.expand_path(__dir__)
    PROJECT_DIR_PATH = File.expand_path(File.dirname(LIB_DIR_PATH))
    TWO_WEEKS_AGO_IN_SECONDS = 2 * 7 * 24 * 60 * 60
    ONE_THOUSAND_GIGABYTES = 1_000 * 1_024 * 1_024 * 1_024

    # Config module
    module Config
      DEFAULTS = {
        log_level: Logger::INFO,
        environment: :staging,
        shards: File.join(PROJECT_DIR_PATH, 'shards.json'),
        output: File.join(PROJECT_DIR_PATH, "#{File.basename($PROGRAM_NAME, '.*')}.json"),
        token_env_variable_names: {
          staging: 'GITLAB_GSTG_ADMIN_API_PRIVATE_TOKEN',
          production: 'GITLAB_GPRD_ADMIN_API_PRIVATE_TOKEN'
        },
        password_prompt: 'Enter Gitlab admin API private token: ',
        api_endpoints: {
          staging: 'https://staging.gitlab.com/api/v4',
          production: 'https://gitlab.com/api/v4'
        },
        projects_api_uri: 'projects',
        projects_per_page: 50,
        projects_last_active_seconds: TWO_WEEKS_AGO_IN_SECONDS,
        project_fields: %i[id path_with_namespace size human_readable_size repository_storage],
        source_shards: [],
        field_renames: {},
        move_amount: ONE_THOUSAND_GIGABYTES,
        maximum_internal_server_error_retries: 3,
        fetch_largest_projects_retry_interval: 5,
        limit: -1
      }.freeze
    end
  end
  # module ProjectSelectorScript

  class UserError < StandardError; end
  class Timeout < StandardError; end
end

# Re-open the Storage module to add the Helpers module
module Storage
  # Helper methods
  module Helpers
    include ::Helpers::Console
    include ::Helpers::Filesize
    # rubocop: disable Style/PercentLiteralDelimiters
    # rubocop: disable Style/RegexpLiteral
    FIVE_HUNDRED_INTERNAL_SERVER_ERROR_PATTERN = %r[500 "Internal Server Error"].freeze
    # rubocop: enable Style/PercentLiteralDelimiters
    # rubocop: enable Style/RegexpLiteral

    def get_api_url(resource_path)
      raise 'No such resource path is configured' unless options.include?(resource_path)

      endpoints = options[:api_endpoints]
      environment = options[:environment]
      endpoint = endpoints.include?(environment) ? endpoints[environment] : endpoints[:production]
      abort 'No api endpoint url is configured' if endpoint.nil? || endpoint.empty?

      [endpoint, options[resource_path]].join('/')
    end

    def symbolize_keys_deep!(memo)
      memo.keys.each do |key|
        symbolized_key = key.respond_to?(:to_sym) ? key.to_sym : key
        memo[symbolized_key] = memo.delete(key) # Preserve order even when key == symbolized_key
        symbolize_keys_deep!(memo[symbolized_key]) if memo[symbolized_key].is_a?(Hash)
      end
    end

    def set_api_token_or_else(&on_failure)
      prompt = options[:password_prompt]
      env_variable_name = options[:token_env_variable_names][options[:environment]]
      token = ENV.fetch(env_variable_name, nil)
      if token.nil? || token.empty?
        log.warn "No #{env_variable_name} variable set in environment"
        token = password_prompt(prompt)
        if token.nil? || token.empty?
          raise 'Failed to get token' unless block_given?

          on_failure.call
        end
      end
      gitlab_api_client.required_headers['Private-Token'] = token
    end

    def projection(memos, fields)
      memos.map { |memo| memo.slice(*fields) }
    end

    def rename_fields(memos, mapping)
      memos.each do |memo|
        mapping.keys.each do |field|
          if mapping.key?(field)
            new_name = mapping[field]
            memo[new_name] = memo.delete(field)
          end
        end
      end
    end

    def repository_size(project)
      repository_size = project[:size] = project.fetch(:statistics, {}).fetch(:repository_size, 0)
      project[:human_readable_size] = to_filesize(repository_size)
      repository_size
    end

    def limit_by_size(projects, max_amount)
      total_bytes = 0
      projects.each_with_object([]) do |project, memo|
        memo << project if project[:size].positive? && (total_bytes += project[:size]) < max_amount
      end
    end
  end
end

# Re-open the Storage module to add the CommandLineSupport module
module Storage
  # Support for command line arguments
  module CommandLineSupport
    # OptionsParser class
    class OptionsParser
      include ::Storage::Helpers
      attr_reader :parser, :options

      def initialize
        @parser = OptionParser.new
        @options = ::Storage::ProjectSelectorScript::Config::DEFAULTS.dup
        define_options
      end

      def define_options
        @parser.banner = "Usage: #{$PROGRAM_NAME} [options] [source-shards, ...]"
        @parser.separator ''
        @parser.separator 'Options:'
        define_head
        define_shards_option
        define_output_option
        define_move_amount_option
        define_limit_option
        define_env_option
        define_verbose_option
        define_tail
      end

      def define_head
        description = "\nWhere [source-shards, ...] is a list of one or more " \
          "names of source gitaly storage shard servers"
        @parser.on_head(description)
      end

      def define_shards_option
        description = "Absolute path to JSON file enumerating shard names; default: #{@options[:shards]}"
        @parser.on('--shards=<file_path>', description) do |file_path|
          unless File.exist?(file_path)
            message = 'Argument given for --shards must be a path to an existing file'
            raise OptionParser::InvalidArgument, message
          end

          @options[:shards] = file_path
        end
      end

      def define_output_option
        description = 'Path at which to export projects or - for stdout; default: ' + @options[:output]
        @parser.on('--output=<fully_qualified_file_path|->', description) do |output|
          file_path = File.expand_path(output)
          dir_path = File.dirname(file_path)
          raise 'Argument --output path is invalid: ' + file_path unless output == '-' || File.exist?(dir_path)

          @options[:output] = file_path
        end
      end

      def define_move_amount_option
        description = "Total repository data in gigabytes limit per shard; default: #{@options[:move_amount]}, or " \
          'largest single repo if 0'
        @parser.on('-m', '--move-amount=<n>', Integer, description) do |move_amount|
          raise 'Size too large' if move_amount > 16_000

          # Convert given gigabytes to bytes
          @options[:move_amount] = (move_amount * 1024 * 1024 * 1024)
        end
      end

      def define_limit_option
        limit = @options[:limit]
        default = limit.positive? ? limit : 'no limit'
        description = "Total project limit for all shards; default: #{default}"
        @parser.on('-l', '--limit=<n>', Integer, description) do |limit|
          @options[:limit] = limit
        end
      end

      def validated_env(env)
        env = env.to_sym
        raise "Invalid environment: #{env}" unless @options[:api_endpoints].key?(env)

        env
      end

      def define_env_option
        description = "Operational environment; default: #{@options[:environment]}"
        @parser.on('--environment=<staging|production>', description) do |env|
          @options[:environment] = validated_env(env)
        end
      end

      def define_verbose_option
        @parser.on('-v', '--verbose', 'Increase logging verbosity') do
          @options[:log_level] -= 1
        end
      end

      def define_tail
        @parser.on_tail('-?', '--help', 'Show this message') do
          puts @parser
          exit
        end
      end
    end
    # class OptionsParser

    def demand(options, arg, type = :positional)
      return options[arg] unless options[arg].nil?

      required_arg = "<#{arg}>"
      required_arg = "--#{arg.to_s.gsub(/_/, '-')}" if type == :optional
      raise UserError, "Required argument: #{required_arg}"
    end

    def parse(args = ARGV, file_path = ARGF)
      opt = OptionsParser.new
      args.push('-?') if args.empty?
      opt.parser.parse!(args)
      # All remaining arguments should be considered source shards
      # specifications
      opt.options[:source_shards].concat(args)
      opt.options
    rescue RuntimeError, OptionParser::InvalidArgument, OptionParser::InvalidOption,
           OptionParser::MissingArgument, OptionParser::NeedlessArgument => e
      puts e.message
      puts opt.parser
      exit 1
    rescue OptionParser::AmbiguousOption => e
      abort e.message
    end
  end
  # module CommandLineSupport
end
# module Storage

# Re-open the Storage module to define the ProjectSelector class
module Storage
  # The ProjectSelector class
  class ProjectSelector
    include ::Helpers::Logging
    include ::Storage::Helpers
    attr_reader :options, :gitlab_api_client

    def initialize(options)
      @options = options
      log.level = @options[:log_level]
      @pagination_indices = {}
      @gitlab_api_client = ::GitLab::Client.new(@options)
    end

    def read_file(file_path)
      data = {}
      begin
        data = IO.read(file_path)
        data = JSON.parse(data) unless data.nil? || data.empty?
      rescue StandardError => e
        log.error 'Error loading projects: ' + e.message
        e.backtrace.each { |t| log.error t }
      end
      data
    end

    def load_shards(file_path = options[:shards])
      read_file(file_path).fetch('shards', [])
    end

    def get_page(url, parameters)
      message = 'Fetching largest projects page %<page>d'
      log.debug format(message, page: parameters.fetch('page', 1))
      results, error, status, headers = gitlab_api_client.get(url, parameters: parameters)
      raise error unless error.nil?

      raise "Invalid response status code: #{status}" unless [200].include?(status)

      raise "Unexpected response: #{results}" if results.nil? || results.empty?

      results.each { |result| symbolize_keys_deep!(result) }
      log.debug JSON.pretty_generate(results)
      [results, headers['x-next-page'].first]
    end

    def pagination_key(url, identifier)
      url + '[' + identifier + ']'
    end

    def fetch_largest_projects(shard)
      log.debug "Fetching largest projects for shard: #{shard}"
      projects = []
      environment = options[:environment]
      max_retries = options[:maximum_internal_server_error_retries]
      retry_interval = options[:fetch_largest_projects_retry_interval]
      url = get_api_url(:projects_api_uri)
      last_active = (Time.now.utc - options[:projects_last_active_seconds]).iso8601
      projects_per_page = options[:projects_per_page]
      limit = options[:limit]
      per_page = limit.positive? ? [limit, projects_per_page].min : projects_per_page
      parameters = {
        order_by: 'repository_size',
        last_activity_before: last_active,
        statistics: true,
        repository_storage: shard,
        per_page: per_page
      }
      pagination_key = pagination_key(url, shard)
      next_page = @pagination_indices[pagination_key]
      if next_page != :no_more_pages
        parameters['page'] = next_page unless next_page.nil?
        iteration = 0
        begin
          projects, next_page = get_page(url, parameters)
          @pagination_indices[pagination_key] = next_page || :no_more_pages
        rescue Net::HTTPFatalError => e
          iteration += 1
          log.error e.message
          # Retry when encountering a 500 Internal Server Error in the staging
          # environment, because that environment's project selection API is
          # unreliable because of the unavailability of cached data for the
          # repository storage information such as repository_size and so on.
          if FIVE_HUNDRED_INTERNAL_SERVER_ERROR_PATTERN.match?(e.message) &&
              environment == :staging &&
              iteration < max_retries
            log.info "Retrying fetch largest projects after server error..."
            sleep retry_interval
            retry
          end
        end
      end
      projects
    end

    def get_projects_from_shard(shard)
      project_fields = options[:project_fields]
      field_renames = options[:field_renames]
      limit = options[:limit]
      move_amount = options[:move_amount]
      projects = []
      total_bytes = 0
      loop do
        page_of_projects = fetch_largest_projects(shard)
        break if page_of_projects.empty?

        projects.concat(page_of_projects)
        total_bytes += page_of_projects.sum(0) { |project| repository_size(project) }
        break if total_bytes >= move_amount || (limit.positive? && projects.length > limit)
      end
      projects = limit_by_size(projects, move_amount)
      projects = projection(projects, project_fields)
      rename_fields(projects, field_renames)
      projects
    end

    def get_projects
      source_shards = options[:source_shards].concat(load_shards)
      limit = options[:limit]
      projects = []
      begin
        projects = source_shards.each_with_object([]) do |source_shard, memo|
          memo.concat(get_projects_from_shard(source_shard)) if limit.negative? || memo.length < limit
        end
        projects = projects[0...options[:limit]] unless limit.negative?
      rescue StandardError => e
        log.error "Error getting projects: " + e.message
        raise e
      end
      projects
    end
  end
  # module CommandLineSupport
end
# module Storage

# Re-open the Storage module to add ProjectSelectorScript module
module Storage
  # ProjectSelectorScript module
  module ProjectSelectorScript
    include ::Helpers::Logging
    include ::Storage::Helpers
    include ::Storage::CommandLineSupport

    def main(args = parse(ARGV, ARGF))
      demand(args, :source_shards)
      selector = ::Storage::ProjectSelector.new(args)
      selector.set_api_token_or_else do
        raise UserError, 'Cannot proceed without a GitLab admin API private token'
      end
      projects = selector.get_projects
      abort "No projects selected" if projects.empty?

      json = JSON.pretty_generate(projects: projects)
      if args[:output] == '-'
        $stdout.write(json)
      else
        File.open(args[:output], 'w') { |f| f.write(json) }
      end
    rescue UserError => e
      abort e.message
    rescue StandardError => e
      log.error e.message
      e.backtrace.each { |trace| log.error trace }
      abort
    rescue SystemExit
      exit
    rescue Interrupt => e
      $stderr.write "\r\n#{e.class}\n"
      $stderr.flush
      $stdin.echo = true if $stdin.respond_to?(:echo=)
      exit 0
    end
  end
  # ProjectSelectorScript module
end
# Storage module

# Anonymous object avoids namespace pollution
Object.new.extend(::Storage::ProjectSelectorScript).main if $PROGRAM_NAME == __FILE__
