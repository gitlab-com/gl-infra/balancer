# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

# Define the Helpers module
module Helpers
  # Define the Filesize module
  module Filesize
    KILOBYTE = 1_024
    BYTES_CONVERSIONS = {
      'B': KILOBYTE,
      'KB': KILOBYTE * KILOBYTE,
      'MB': KILOBYTE * KILOBYTE * KILOBYTE,
      'GB': KILOBYTE * KILOBYTE * KILOBYTE * KILOBYTE,
      'TB': KILOBYTE * KILOBYTE * KILOBYTE * KILOBYTE * KILOBYTE
    }.freeze

    def to_filesize(bytes)
      BYTES_CONVERSIONS.each_pair do |denomination, threshold|
        next unless bytes < threshold

        threshold_kilobytes = (threshold.to_f / KILOBYTE)
        filesize = (bytes.to_f / threshold_kilobytes).round(2)
        return "#{filesize} #{denomination}"
      end
    end
  end
  # module Filesizes
end
# module Helpers
