# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

# Define Helpers::Console module
module Helpers
  # This module defines Console methods
  module Console
    DEFAULT_PASSWORD_PROMPT = 'Enter password: '

    def password_prompt(prompt = nil)
      prompt = prompt.nil? || prompt.empty? ? DEFAULT_PASSWORD_PROMPT : prompt
      $stdout.write(prompt)
      $stdout.flush
      $stdin.noecho(&:gets).chomp
    ensure
      $stdin.echo = true if $stdin.respond_to?(:echo=)
      # $stdout.flush
      $stdout.ioflush if $stdin.respond_to?(:ioflush)
      $stdout.write "\r" + (' ' * prompt.length) + "\n"
      $stdout.flush
    end

    def execute_command(command)
      log.debug "Executing command: #{command}"
      `#{command}`.strip
    end
  end
  # module Console
end
# module Helpers
