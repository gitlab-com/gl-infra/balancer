# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

# Define the Helpers module to add the Debugging module
module Helpers
  # This module defines debugging methods
  module Debugging
    HEADER_ARGUMENT_TEMPLATE = '--header "%<field>s: %<value>s"'
    PRIVATE_TOKEN_HEADER_PATTERN = /Private-Token/i.freeze

    def debug_command(cmd)
      log.debug "Command: #{cmd}"
      cmd
    end

    def get_request_headers(request)
      request.instance_variable_get('@header'.to_sym) || []
    end

    # rubocop: disable Style/PercentLiteralDelimiters
    # rubocop: disable Style/RegexpLiteral
    def to_unescaped_json(data)
      return data unless data.respond_to?(:to_json)

      data.to_json.gsub(%r[\\"], '"').gsub(%r[^"{], '{').gsub(%r[}"$], '}')
    end
    # rubocop: enable Style/PercentLiteralDelimiters
    # rubocop: enable Style/RegexpLiteral

    # This method reproduces a Net:HTTP request as a verbatim
    # port to a curl command, executable on the command line,
    # in order to make a given web-request portable.
    def debug_request(request)
      environment = options[:environment]
      token_env_variable_names = options.fetch(:token_env_variable_names, {})
      env_variable_name = token_env_variable_names.fetch(environment, nil)
      headers = get_request_headers(request)
      log.debug "[The following curl command is for external diagnostic purposes only:]"
      curl_command = "curl --verbose --silent --compressed ".dup
      curl_command = curl_command.concat("--request #{request.method.to_s.upcase} ") if request.method != :get
      curl_command = curl_command.concat("'#{request.uri}'")
      header_arguments = headers.collect do |field, values|
        values = [format('${%<var>s}', var: env_variable_name)] if !env_variable_name.nil? &&
          PRIVATE_TOKEN_HEADER_PATTERN.match?(field)
        format(HEADER_ARGUMENT_TEMPLATE, field: field, value: values.join(','))
      end
      unless header_arguments.empty?
        curl_command = curl_command.concat(' ')
        curl_command = curl_command.concat(header_arguments.join(' '))
      end
      body = request.body
      curl_command = curl_command.concat(" --data '#{to_unescaped_json(body)}'") unless body.nil? || body.empty?
      log.debug curl_command
    end

    def debug_lines(lines)
      return if lines.empty?

      log.debug do
        lines.each { |line| log.debug line unless line.nil? || line.empty? }
      end
    end
  end
  # module Debugging
end
# module Storage
