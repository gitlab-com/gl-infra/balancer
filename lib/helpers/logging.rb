# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

require 'logger'

module Helpers
  # This module defines logging methods
  module Logging
    LOG_TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S'
    MIGRATION_TIMESTAMP_FORMAT = '%Y-%m-%d_%H%M%S'
    LOG_FORMAT = "%<timestamp>s %-5<level>s %<msg>s\n"
    PROGRESS_LOG_FORMAT = "\r%<timestamp>s %-5<level>s %<msg>s"
    LOG_META_INFO_LENGTH = 26
    DISPLAY_WIDTH = 80 - LOG_META_INFO_LENGTH
    SEPARATOR = ('=' * DISPLAY_WIDTH).freeze

    def formatter_procedure(format_template = LOG_FORMAT)
      proc do |level, t, _name, msg|
        format(
          format_template,
          timestamp: t.strftime(LOG_TIMESTAMP_FORMAT),
          level: level, msg: msg)
      end
    end

    def initialize_log(formatter = formatter_procedure)
      $stdout.sync = true
      log = Logger.new($stdout)
      log.level = Logger::INFO
      log.formatter = formatter
      log
    end

    def log
      @log ||= initialize_log
    end

    def progress_log
      @progress_log ||= initialize_log(formatter_procedure(PROGRESS_LOG_FORMAT))
    end

    def with_log_level(log_level = Logger::INFO, &block)
      sv_log_level = log.level
      log.level = log_level
      block.call
    ensure
      log.level = sv_log_level
    end

    def log_separator
      log.info(SEPARATOR)
    end

    def dry_run_notice
      log.info '[Dry-run] This is only a dry-run -- write operations will be logged but not ' \
        'executed'
    end

    def init_project_migration_logging(
      logfile_name = options[:migration_logfile_name], log_level = Logger::INFO)
      fields = { date: Time.now.strftime(MIGRATION_TIMESTAMP_FORMAT) }
      logfile_name = format(logfile_name, **fields)
      log_dir_path = options[:log_dir_path]
      FileUtils.mkdir_p(log_dir_path)
      log_file_path = File.join(log_dir_path, logfile_name)
      FileUtils.touch(log_file_path)
      logger = Logger.new(log_file_path, level: log_level)
      logger.formatter = proc { |level, t, name, msg| "#{msg}\n" }
      log.debug "Migration log file path: #{log_file_path}"
      logger
    rescue StandardError => e
      log.error "Failed to configure logging: #{e.message}"
      exit
    end

    def migration_log
      @migration_log ||= init_project_migration_logging
    end

    def migration_error_log
      @migration_error_log ||= init_project_migration_logging(options[:migration_error_logfile_name], Logger::ERROR)
    end
  end
  # module Logging
end
# module Helpers
