# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

require 'json'
require 'net/http'
require 'ostruct'
require 'uri'

require_relative '../helpers/debugging'
require_relative '../helpers/logging'

# Define the GitLab module
module GitLab
  # Define the ::GitLab::Client class
  class Client
    DEFAULT_RESPONSE = OpenStruct.new(code: 400, body: '{}') unless defined? ::GitLab::Client::DEFAULT_RESPONSE
    include ::Helpers::Debugging
    include ::Helpers::Logging
    attr_reader :options
    attr_accessor :required_headers

    def initialize(options)
      @options = options
      log.level = @options[:log_level]
      @required_headers = {}
    end

    def get(url, opts = {})
      request(Net::HTTP::Get, url, opts)
    end

    def post(url, opts = {})
      opts.update(headers: { 'Content-Type': 'application/json' }) if opts.fetch(
        :body, nil).respond_to?(:[]) && !opts.fetch(:headers, {}).include?('Content-Type')
      request(Net::HTTP::Post, url, opts)
    end

    def put(url, opts = {})
      request(Net::HTTP::Put, url, opts)
    end

    private

    def request(klass, url, opts = {})
      uri = URI(url)
      parameters = opts.fetch(:parameters, {}).transform_keys(&:to_s).transform_values(&:to_s)
      uri.query = URI.encode_www_form(parameters) unless parameters.empty?
      client = Net::HTTP.new(uri.host, uri.port)
      client.use_ssl = (uri.scheme == 'https')
      headers = opts.fetch(:headers, {}).merge(required_headers)
      request = klass.new(uri, headers)
      invoke(client, request, opts)
    end

    # rubocop: disable Metrics/AbcSize
    def invoke(client, request, opts = {})
      body = opts[:body]
      request.body = body.respond_to?(:bytesize) ? body : body.to_json unless body.nil?
      debug_request(request)

      response = ::GitLab::Client::DEFAULT_RESPONSE
      result = {}
      error = nil
      status = response.code
      begin
        response, status = execute(client, request)
      rescue Errno::ECONNREFUSED => e
        log.error e.to_s
        error = e
      rescue EOFError => e
        log.error "Encountered EOF reading from network socket"
        error = e
      rescue OpenSSL::SSL::SSLError => e
        log.error "Socket error: #{e} (#{e.class})"
        error = e
      rescue Net::ReadTimeout => e
        log.error "Timed out reading: " + e.message
        error = e
      rescue Net::OpenTimeout => e
        log.error "Timed out opening connection: " + e.message
        error = e
      rescue Net::HTTPBadResponse => e
        log.error "Bad response: " + e.message
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPUnauthorized => e
        log.error "Authorization failure: " + e.message
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPClientException => e
        log.error "Unexpected HTTP client exception: #{e.message} (#{e.class})"
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPClientError => e
        log.error "Unexpected HTTP client error: #{e.message} (#{e.class})"
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPServerException => e
        log.error "Unexpected HTTP server error: " + e.message
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPFatalError => e
        log.error "Unexpected HTTP fatal error: #{e.message}"
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::ProtocolError => e
        log.error "Unexpected HTTP protocol error: #{e.message} (#{e.class})"
        error = e
        status = e.response.code.to_i if e.respond_to?(:response)
      rescue Net::HTTPFatalError => e
        log.error "Unexpected HTTP fatal error: #{e.message} (#{e.class})"
        error = e
      rescue IOError => e
        log.error "Unexpected IO error: #{e} (#{e.class})"
        error = e
      rescue StandardError => e
        log.error "Unexpected error: #{e} (#{e.class})"
        log.error e.exception_type if e.respond_to? :exception_type
        error = e
      end

      headers = {}
      response.each_header { |key, value| headers[key] = value.split(', ') }
      result, error = deserialize(response) if error.nil?

      [result, error, status, headers]
    end
    # rubocop: enable Metrics/AbcSize

    def execute(client, request)
      response = client.request(request)
      log.debug "Response status code: #{response.code}"
      response.value
      [response, response.code.to_i]
    end

    def deserialize(response)
      result = nil
      error = nil
      begin
        response_data = response.body
        result = JSON.parse(response_data)
      rescue JSON::ParserError => e
        n = response.body.length
        message = "Could not parse #{n} bytes of json serialized data from #{uri.path}"
        if n > 65536
          log.warn message
        else
          log.error message
          error = e
        end
      rescue IOError => e
        log.error format('Unexpected IO error: %<error>s (%<error_class>s)', error: e, error_class: e.class)
        error = e
      rescue StandardError => e
        log.error format(
          'Unexpected error: %<error>s (%<error_class>s%<error_type>s)',
          error: e, error_class: e.class,
          error_type: e.respond_to?(:exception_type) ? format('/%s', e.exception_type) : '')
        error = e
      end
      [result, error]
    end
  end
  # class Client
end
# module GitLab
