# frozen_string_literal: true

require 'spec_helper'

require_relative '../lib/gitlab/client.rb'

describe ::GitLab::Client do
  subject { described_class.new(options) }
  let(:defaults) { ::Storage::MoverScript::Config::DEFAULTS.dup }
  let(:options) { defaults.merge({ gitlab_admin_api_token: test_token }) }
  let(:test_token) { 'test' }

  let(:test_request) { double('Net::HTTPRequest') }

  let(:test_status_code) { test_response_code_ok }
  let(:test_response_code_ok) { 200 }
  let(:test_response_body) do
    body = {}
    body['resource'] = 'value'
    body
  end
  let(:test_response_headers) { {} }
  let(:test_response_body_serialized_json) { test_response_body.to_json }
  let(:test_error) { nil }
  let(:test_response_successful) do
    response = double('Net::HTTP::Response')
    allow(response).to receive(:code).and_return(test_response_code_ok)
    allow(response).to receive(:body).and_return(test_response_body_serialized_json)
    allow(response).to receive(:each_header)
    response
  end
  let(:test_response_code_not_found) { 404 }
  let(:test_client_error_message) { 'Unexpected HTTP client exception: ' }
  let(:test_client_error_class) { Net::HTTPServerException }
  let(:test_not_found_message) { 'NotFound' }
  let(:test_http_not_found_error) { Net::HTTPClientException.new(test_not_found_message, test_response_not_found) }
  let(:test_response_not_found) do
    response = double('Net::HTTP::Response')
    allow(response).to receive(:code).and_return(test_response_code_not_found)
    allow(response).to receive(:body).and_return(test_response_body_serialized_json)
    response
  end

  let(:test_headers) { ['test: header'] }
  let(:net_http) do
    net_http = double('Net::HTTP')
    allow(net_http).to receive(:use_ssl=).with(true)
    net_http
  end
  let(:get_url) { 'https://test.com/api/resource.json' }
  let(:put_url) { 'https://test.com/api/resource.json' }
  let(:post_url) { 'https://test.com/api/resource.json' }

  RSpec::Matchers.define :an_http_request do |x|
    match { |actual| actual.is_a?(Net::HTTPRequest) }
  end

  before do
    allow(subject).to receive(:execute).with(net_http, an_http_request).and_return([test_response, test_status_code])
    allow(Net::HTTP).to receive(:new).and_return(net_http)
  end

  context 'when GET https://test.com/api/resource.json' do
    let(:test_method) { Net::HTTP::Get }
    let(:test_response) { test_response_successful }

    it 'returns the deserialized resource with no error and status code 200' do
      expect(subject.get(get_url)).to eq([test_response_body, test_error, test_status_code, test_response_headers])
    end

    context 'when the requested resource does not exist' do
      let(:test_response) { test_response_not_found }
      let(:test_status_code) { test_response_code_not_found }
      let(:test_response_body) { {} }
      let(:test_error) { test_http_not_found_error }

      it 'returns an empty hash with the error and status code 404' do
        expect(subject).to receive(:execute).and_raise(test_http_not_found_error)
        expect(subject.log).to receive(:error).with(
          test_client_error_message + test_not_found_message + ' (' + test_client_error_class.name + ')')
        expect(subject.get(get_url)).to eq([test_response_body, test_error, test_status_code, test_response_headers])
      end
      # it 'returns an empty hash with the error and status code 404'
    end
    # context 'when the requested resource does not exist'
  end
  # context 'when GET https://test.com/api/resource.json'

  context 'when PUT https://test.com/api/resource.json' do
    let(:test_method) { Net::HTTP::Put }
    let(:test_response) { test_response_successful }

    it 'returns the deserialized resource with no error and status code 200' do
      expect(subject.put(put_url)).to eq([test_response_body, nil, 200, test_response_headers])
    end

    context 'when the requested resource does not exist' do
      let(:test_response) { test_response_not_found }
      let(:test_status_code) { test_response_code_not_found }
      let(:test_response_body) { {} }
      let(:test_error) { test_http_not_found_error }

      it 'returns an empty hash with the error and status code 404' do
        expect(subject).to receive(:execute).and_raise(test_http_not_found_error)
        expect(subject.log).to receive(:error).with(
          test_client_error_message + test_not_found_message + ' (' + test_client_error_class.name + ')')
        expect(subject.get(get_url)).to eq([test_response_body, test_error, test_status_code, test_response_headers])
      end
      # it 'returns an empty hash with the error and status code 404'
    end
    # context 'when the requested resource does not exist'
  end
  # context 'when PUT https://test.com/api/resource.json'

  context 'when POST https://test.com/api/resource.json' do
    let(:test_method) { Net::HTTP::Post }
    let(:test_response) { test_response_successful }

    it 'returns the deserialized resource with no error and status code 200' do
      expect(subject.post(post_url)).to eq([test_response_body, nil, 200, test_response_headers])
    end

    context 'when the requested resource does not exist' do
      let(:test_response) { test_response_not_found }
      let(:test_status_code) { test_response_code_not_found }
      let(:test_response_body) { {} }
      let(:test_error) { test_http_not_found_error }

      it 'returns an empty hash with the error and status code 404' do
        expect(subject).to receive(:execute).and_raise(test_http_not_found_error)
        expect(subject.log).to receive(:error).with(
          test_client_error_message + test_not_found_message + ' (' + test_client_error_class.name + ')')
        expect(subject.get(get_url)).to eq([test_response_body, test_error, test_status_code, test_response_headers])
      end
      # it 'returns an empty hash with the error and status code 404'
    end
    # context 'when the requested resource does not exist'
  end
  # context 'when POST https://test.com/api/resource.json'
end
# describe ::Storage::GitLabClient
