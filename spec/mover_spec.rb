# frozen_string_literal: true

require 'spec_helper'

require_relative '../lib/mover.rb'

describe ::Storage::Mover do
  subject { described_class.new(options) }
  let(:test_node_01) { 'nfs-file03' }
  let(:test_node_02) { 'nfs-file04' }
  let(:test_private_token) { 'test_token' }
  let(:test_commit_id) { 1 }
  let(:test_project_id) { 1 }
  let(:dry_run) { true }
  let(:args) do
    { source_shard: test_node_01, dry_run: dry_run }
  end
  let(:defaults) { ::Storage::MoverScript::Config::DEFAULTS.dup.merge(args) }
  let(:options) { defaults }
  let(:projects) { [{ id: test_project_id }] }

  describe '#move' do
    let(:projects) { [test_project] }
    let(:test_repository_size) { 1 * 1024 * 1024 * 1024 }
    let(:test_project_commit_count) { 1 }
    let(:largest_denomination) { '1 GB' }
    let(:gitaly_address_fqdn_01) { 'test.01' }
    let(:gitaly_address_fqdn_02) { 'test.02' }
    let(:gitaly_address_01) { "test://#{gitaly_address_fqdn_01}" }
    let(:gitaly_address_02) { "test://#{gitaly_address_fqdn_02}" }
    let(:node_configuration) do
      { test_node_01 => { 'gitaly_address' => gitaly_address_01 },
        test_node_02 => { 'gitaly_address' => gitaly_address_02 } }
    end
    let(:test_project_name) { 'test_project_name' }
    let(:test_project_path_with_namespace) { 'test/test_project_name' }
    let(:test_project_disk_path) { 'test/project_disk_path' }
    let(:test_repository) do
      repository = double('Repository')
      allow(repository).to receive(:expire_exists_cache)
      repository
    end
    let(:test_project) do
      {
        id: test_project_id,
        name: test_project_name,
        path_with_namespace: test_project_path_with_namespace,
        disk_path: test_project_disk_path,
        statistics: {
          repository_size: test_repository_size
        },
        repository_storage: test_node_01
      }
    end
    let(:test_project_json) { test_project.to_json }
    let(:test_projects) { { projects: [test_project] } }
    let(:test_projects_json) { test_projects.to_json }
    let(:test_full_project) do
      { id: test_project_id, name: test_project_name, path_with_namespace: test_project_path_with_namespace,
        disk_path: test_project_disk_path, repository_storage: test_node_01 }
    end
    let(:test_project_put_response) { { 'id': test_project_id }.transform_keys(&:to_s) }
    let(:test_updated_full_project) do
      { id: test_project_id, name: test_project_name, path_with_namespace: test_project_path_with_namespace,
        disk_path: test_project_disk_path, repository_storage: test_node_02 }
    end
    let(:test_moves) { [{ 'project': { 'id': test_project_id }, 'state': 'finished' }] }
    let(:test_move) { { 'project': { 'id': test_project_id }, 'state': 'started' } }
    let(:test_migration_logger) { double('FileLogger') }
    let(:test_time) { DateTime.now.iso8601(::Storage::Helpers::ISO8601_FRACTIONAL_SECONDS_LENGTH) }
    let(:test_artifact) do
      {
        id: test_project_id,
        path: test_project_disk_path,
        # size: test_repository_size,
        source: test_node_01,
        date: test_time
      }
    end
    let(:test_migration_failure_id) { 1234567890 }
    let(:test_migration_failures) do
      [
        {
          project_id: test_migration_failure_id,
          message: "Noticed service failure during repository replication",
          disk_path: "@hashed/12/34/1234567890abcdefghijklmnopqrstuvwxyz1234567890abcdefghijklmnopqr",
          source: test_node_01,
          date: test_time
        }
      ]
    end
    let(:test_hostname) { options[:console_nodes][:production] }
    let(:options) do
      defaults.merge(
        source_shard: test_node_01,
        dry_run: dry_run
      )
    end

    before do
      allow(subject).to receive(:read_file).and_return(test_projects)
      allow(subject).to receive(:init_project_migration_logging).and_return(test_migration_logger)
      allow(subject).to receive(:options).and_return(options)
      allow(subject).to receive(:get_commit_id).and_return(test_commit_id)
      allow(subject).to receive(:fetch_repository_storage_moves).and_return(test_moves)
      allow(subject).to receive(:load_migration_failures).and_return(test_migration_failures)
      allow_any_instance_of(DateTime).to receive(:iso8601)
        .with(::Storage::Helpers::ISO8601_FRACTIONAL_SECONDS_LENGTH)
        .and_return(test_time)
    end

    context 'when the --dry-run option is true' do
      before do
        allow(subject).to receive(:fetch_project).and_return(test_project)
        allow(subject).to receive(:fetch_largest_projects).and_return([test_project])
      end

      it 'logs the move operation' do
        # allow(subject).to receive(:paginate_projects).and_yield(test_project)

        expect(subject.log).to receive(:info).with(
          'User has specified neither --limit nor --move-amount and has given no projects')
        expect(subject.log).to receive(:info).with('Balancer will only move 1 fetched project.')
        expect(subject.log).to receive(:info).with('Fetching largest projects')
        expect(subject.log).to receive(:info).with("Filtering #{test_migration_failures.length} " \
          "known failed project repositories")
        expect(subject.log).to receive(:info).with(::Storage::MoverScript::SEPARATOR)
        expect(subject.log).to receive(:info).with("Scheduling repository move for project id: " \
          "#{test_project_id}")
        expect(subject.log).to receive(:info).with("  Project path: #{test_project_path_with_namespace}")
        expect(subject.log).to receive(:info).with("  Current shard name: #{test_node_01}")
        expect(subject.log).to receive(:info).with("  Disk path: #{test_project_disk_path}")
        expect(subject.log).to receive(:info).with("  Repository size: 1.0 GB")
        expect(subject.log).to receive(:info).with("[Dry-run] Would have scheduled repository " \
          "move for project id: #{test_project_id}")
        expect(subject.log).to receive(:info).with(::Storage::MoverScript::SEPARATOR)
        expect(subject.log).to receive(:info).with('[Dry-run] Would have processed 1.0 GB of data')
        expect(subject.log).to receive(:info).with('Done')
        expect(subject.move).to be_nil
      end
      # it 'logs the move operation'

      context 'when the projects are not given' do
        it 'selects the projects from the configured console node' do
          expect(subject.log).to receive(:info)
            .with('User has specified neither --limit nor --move-amount and has given no projects')
          expect(subject.log).to receive(:info).with('Balancer will only move 1 fetched project.')
          expect(subject.log).to receive(:info).with('Fetching largest projects')
          expect(subject.log).to receive(:info).with("Filtering #{test_migration_failures.length} " \
            "known failed project repositories")
          expect(subject.log).to receive(:info).with(::Storage::MoverScript::SEPARATOR)
          expect(subject.log).to receive(:info).with("Scheduling repository move for project id: " \
            "#{test_project_id}")
          expect(subject.log).to receive(:info).with("  Project path: #{test_project_path_with_namespace}")
          expect(subject.log).to receive(:info).with("  Current shard name: #{test_node_01}")
          expect(subject.log).to receive(:info).with("  Disk path: #{test_project_disk_path}")
          expect(subject.log).to receive(:info).with("  Repository size: 1.0 GB")
          expect(subject.log).to receive(:info).with("[Dry-run] Would have scheduled repository " \
            "move for project id: #{test_project_id}")
          expect(subject.log).to receive(:info).with(::Storage::MoverScript::SEPARATOR)
          expect(subject.log).to receive(:info).with('[Dry-run] Would have processed 1.0 GB of data')
          expect(subject.log).to receive(:info).with('Done')
          expect(subject.move).to be_nil
        end
        # it 'selects the projects from the configured console node'
      end
      # context 'when the projects are not given'

      context 'when the destination shard is not given' do
        let(:test_node_02) { nil }

        it 'lets the application select the destination shard' do
          expect(subject.log).to receive(:info)
            .with('User has specified neither --limit nor --move-amount and has given no projects')
          expect(subject.log).to receive(:info).with('Balancer will only move 1 fetched project.')
          expect(subject.log).to receive(:info).with('Fetching largest projects')
          expect(subject.log).to receive(:info).with("Filtering #{test_migration_failures.length} " \
            "known failed project repositories")
          expect(subject.log).to receive(:info).with(::Storage::MoverScript::SEPARATOR)
          expect(subject.log).to receive(:info).with("Scheduling repository move for project id: " \
            "#{test_project_id}")
          expect(subject.log).to receive(:info).with("  Project path: #{test_project_path_with_namespace}")
          expect(subject.log).to receive(:info).with("  Current shard name: #{test_node_01}")
          expect(subject.log).to receive(:info).with("  Disk path: #{test_project_disk_path}")
          expect(subject.log).to receive(:info).with("  Repository size: 1.0 GB")
          expect(subject.log).to receive(:info).with("[Dry-run] Would have scheduled repository " \
            "move for project id: #{test_project_id}")
          expect(subject.log).to receive(:info).with(::Storage::MoverScript::SEPARATOR)
          expect(subject.log).to receive(:info).with('[Dry-run] Would have processed 1.0 GB of data')
          expect(subject.log).to receive(:info).with('Done')
          expect(subject.move).to be_nil
        end
        # let 'lets the application select the destination shard'
      end
      # context 'when the destination shard is not given'
    end
    # context 'when the --dry-run option is true'

    context 'when the --dry-run option is false' do
      let(:dry_run) { false }

      before do
        allow(subject).to receive(:fetch_project).and_return(test_project)
        allow(subject).to receive(:fetch_largest_projects).and_return([test_project])
      end

      it 'logs the move operation' do
        # allow(subject).to receive(:paginate_projects).and_yield(test_project)
        allow(subject).to receive(:create_repository_storage_move).and_return(test_move)
        allow(subject).to receive(:fetch_repository_storage_move).and_return(test_move)

        expect(subject.log).to receive(:info)
          .with('User has specified neither --limit nor --move-amount and has given no projects')
        expect(subject.log).to receive(:info).with('Balancer will only move 1 fetched project.')
        expect(subject.log).to receive(:info).with('Fetching largest projects')
        expect(subject.log).to receive(:info).with("Filtering #{test_migration_failures.length} " \
          "known failed project repositories")
        expect(subject.log).to receive(:info).with(::Storage::MoverScript::SEPARATOR)
        expect(subject.log).to receive(:info).with("Scheduling repository move for project id: " \
          "#{test_project_id}")
        expect(subject.log).to receive(:info).with("  Project path: #{test_project_path_with_namespace}")
        expect(subject.log).to receive(:info).with("  Current shard name: #{test_node_01}")
        expect(subject.log).to receive(:info).with("  Disk path: #{test_project_disk_path}")
        expect(subject.log).to receive(:info).with("  Repository size: 1.0 GB")
        expect(subject).to receive(:loop_with_progress_until).and_yield.and_yield
        expect(subject).to receive(:fetch_project).and_return(test_full_project)
        expect(subject).to receive(:fetch_project).and_return(test_updated_full_project)
        expect(subject).to receive(:fetch_repository_storage_moves).and_return(test_moves)
        expect(subject.log).to receive(:info).with("Success moving project id: #{test_project_id} to #{test_node_02}")
        expect(test_migration_logger).to receive(:info).with(test_artifact.to_json)
        expect(subject.log).to receive(:info).with("Migrated project id: #{test_project_id}")
        expect(subject.log).to receive(:info).with(::Storage::MoverScript::SEPARATOR)
        expect(subject.log).to receive(:info).with('Done')
        expect(subject.log).to receive(:info).with('Rebalancing done. Processed 1.0 GB of data')
        expect(subject.log).to receive(:info).with("Finished migrating projects")
        expect(subject.log).to receive(:info).with('No errors encountered during migration')
        expect(subject.move).to be_nil
      end
      # it 'logs the move operation'

      context 'when the projects are not given' do
        it 'selects the projects from the configured console node' do
          allow(subject).to receive(:create_repository_storage_move).and_return(test_move)
          allow(subject).to receive(:fetch_repository_storage_move).and_return(test_move)

          expect(subject.log).to receive(:info)
            .with('User has specified neither --limit nor --move-amount and has given no projects')
          expect(subject.log).to receive(:info).with('Balancer will only move 1 fetched project.')
          expect(subject.log).to receive(:info).with('Fetching largest projects')
          expect(subject.log).to receive(:info).with("Filtering #{test_migration_failures.length} " \
            "known failed project repositories")
          expect(subject.log).to receive(:info).with(::Storage::MoverScript::SEPARATOR)
          expect(subject.log).to receive(:info).with("Scheduling repository move for project id: " \
            "#{test_project_id}")
          expect(subject.log).to receive(:info).with("  Project path: #{test_project_path_with_namespace}")
          expect(subject.log).to receive(:info).with("  Current shard name: #{test_node_01}")
          expect(subject.log).to receive(:info).with("  Disk path: #{test_project_disk_path}")
          expect(subject.log).to receive(:info).with("  Repository size: 1.0 GB")
          expect(subject).to receive(:create_repository_storage_move).with(test_project, nil).and_return(test_move)
          expect(subject).to receive(:loop_with_progress_until).and_yield.and_yield
          expect(subject).to receive(:fetch_project).and_return(test_full_project)
          allow(subject).to receive(:fetch_repository_storage_moves).and_return(test_moves)
          allow(subject).to receive(:fetch_repository_storage_moves).and_return(test_moves)
          expect(subject.log).to receive(:info).with("Success moving project id: #{test_project_id} to #{test_node_02}")
          expect(subject).to receive(:fetch_project).and_return(test_updated_full_project)
          expect(test_migration_logger).to receive(:info).with(test_artifact.to_json)
          expect(subject.log).to receive(:info).with("Migrated project id: #{test_project_id}")
          expect(subject.log).to receive(:info).with(::Storage::MoverScript::SEPARATOR)
          expect(subject.log).to receive(:info).with('Done')
          expect(subject.log).to receive(:info).with('Rebalancing done. Processed 1.0 GB of data')
          expect(subject.log).to receive(:info).with("Finished migrating projects")
          expect(subject.log).to receive(:info).with('No errors encountered during migration')
          expect(subject.move).to be_nil
        end
        # it 'selects the projects from the configured console node'
      end
      # context 'when the projects are not given'

      context 'when the destination shard is not given' do
        let(:test_node_02) { nil }

        it 'lets the application select the destination shard' do
          allow(subject).to receive(:create_repository_storage_move).and_return(test_move)
          allow(subject).to receive(:fetch_repository_storage_move).and_return(test_move)

          expect(subject.log).to receive(:info)
            .with('User has specified neither --limit nor --move-amount and has given no projects')
          expect(subject.log).to receive(:info).with('Balancer will only move 1 fetched project.')
          expect(subject.log).to receive(:info).with('Fetching largest projects')
          expect(subject.log).to receive(:info).with("Filtering #{test_migration_failures.length} " \
            "known failed project repositories")
          expect(subject.log).to receive(:info).with(::Storage::MoverScript::SEPARATOR)
          expect(subject.log).to receive(:info).with("Scheduling repository move for project id: " \
            "#{test_project_id}")
          expect(subject.log).to receive(:info).with("  Project path: #{test_project_path_with_namespace}")
          expect(subject.log).to receive(:info).with("  Current shard name: #{test_node_01}")
          expect(subject.log).to receive(:info).with("  Disk path: #{test_project_disk_path}")
          expect(subject.log).to receive(:info).with("  Repository size: 1.0 GB")
          expect(subject).to receive(:create_repository_storage_move).with(test_project, nil).and_return(test_move)
          expect(subject).to receive(:loop_with_progress_until).and_yield.and_yield
          expect(subject).to receive(:fetch_project).and_return(test_full_project)
          allow(subject).to receive(:fetch_repository_storage_moves).and_return(test_moves)
          allow(subject).to receive(:fetch_repository_storage_moves).and_return(test_moves)
          expect(subject.log).to receive(:info).with("Success moving project id: #{test_project_id} to #{test_node_02}")
          expect(subject).to receive(:fetch_project).and_return(test_updated_full_project)
          expect(test_migration_logger).to receive(:info).with(test_artifact.to_json)
          expect(subject.log).to receive(:info).with("Migrated project id: #{test_project_id}")
          expect(subject.log).to receive(:info).with(::Storage::MoverScript::SEPARATOR)
          expect(subject.log).to receive(:info).with('Done')
          expect(subject.log).to receive(:info).with('Rebalancing done. Processed 1.0 GB of data')
          expect(subject.log).to receive(:info).with("Finished migrating projects")
          expect(subject.log).to receive(:info).with('No errors encountered during migration')
          expect(subject.move).to be_nil
        end
        # let 'lets the application select the destination shard'
      end
      # context 'when the destination shard is not given'
    end
    # context 'when the --dry-run option is false'
  end
  # describe '#move'
end
# describe ::Storage::Mover

describe ::Storage::MoverScript do
  subject { Object.new.extend(::Storage::MoverScript) }
  let(:test_project_id) { 1 }
  let(:test_node_01) { 'nfs-file03' }
  let(:test_node_02) { 'nfs-file04' }
  let(:dry_run) { true }
  let(:args) do
    { source_shard: test_node_01,
      projects: projects, dry_run: dry_run }
  end
  let(:defaults) { ::Storage::MoverScript::Config::DEFAULTS.dup.merge(args) }
  let(:options) { defaults }
  let(:mover) { double('::Storage::Mover') }
  let(:projects) { [{ id: test_project_id }] }
  let(:test_token) { 'test' }
  let(:no_token_message) { 'Cannot proceed without a GitLab admin API private token' }

  before do
    allow(::Storage::Mover).to receive(:new).and_return(mover)
    allow(mover).to receive(:set_api_token_or_else)
    allow(subject).to receive(:parse).and_return(options)
    token_env_variable_name = options[:token_env_variable_name]
    allow(ENV).to receive(:[]).with(token_env_variable_name).and_return(test_token)
  end

  describe '#main' do
    context 'when no token is provided' do
      let(:test_token) { nil }

      it 'aborts and whines about it' do
        expect(subject.log).to receive(:info).with('[Dry-run] This is only a dry-run -- write ' \
          'operations will be logged but not executed')
        expect(mover).to receive(:set_api_token_or_else).and_yield
        expect { subject.main }.to raise_error(SystemExit, no_token_message).and(
          output(Regexp.new(no_token_message)).to_stderr)
      end
      # it 'aborts and whines about it'
    end
    # context 'when no token is provided'

    context 'when the dry-run option is true' do
      it 'logs the given operation' do
        expect(subject.log).to receive(:info).with('[Dry-run] This is only a dry-run -- write ' \
          'operations will be logged but not executed')
        expect(mover).to receive(:move)
        expect(subject.main).to be_nil
      end
      # it 'logs the given operation'
    end
    # context 'when the dry-run option is true'

    context 'when the dry-run option is false' do
      let(:options) { defaults.merge(dry_run: false) }

      it 'safely invokes the given operation' do
        expect(subject).to receive(:parse).and_return(options)
        expect(mover).to receive(:move)
        expect(subject.log).not_to receive(:info)
        expect(subject.main).to be_nil
      end
      # it 'safely invokes the given operation'
    end
    # context 'when the dry-run option is false'
  end
  # describe '#main'
end
# describe ::Storage::MoverScript
