# Gitaly shard project repository balancer

Should detect overloaded gitaly shards and schedule the movement of 1000 GB
worth of project repositories from each overloaded shard to whichever shard
is selected by the shard weights configured automatically in the gitlab
application.

To be clear, a shard is just a gitaly node.  A gitaly node is just a host
running Ubuntu with the gitaly application running as a service.  The
gitaly application acts as an abstraction layer for git repository
operations required for GitLab features. Each gitaly node holds some of
the project repository inventory managed by the gitlab application
instance.

## Using Balancer through CI

In ops.gitlab.net, we run a scheduled job that balances Gitaly shards, but a manual job can also be kicked off. To kick off a manual job, the following variables are needed for a manual job:

* `ENVIRONMENT`: Default is staging. Options are `production` or `staging`.
* `DRY_RUN`: Defaults to true. Set this as `false` or `no` if you want to run a migration.
* `SLACK_NOTIFY`: Set this to true if you'd like to notify the `#production` channel. We don't recommend setting this environment variable if you're migrating staging shards or running rspec tests through the CI on ops.gitlab.net as some of the `move` specs are not run in "dry run" mode.

Other variables/options that can also be configured:

* `SHARD_LIMIT`: Default is `-1` (no limit). The number of shards you want to balance. We recommend doing 1 per job due to the trace log limits.
* `MOVE_AMOUNT`: Total repository data to move in gigabytes (limit per shard, not total). Default is `1000` GiB. Max is capped at `16000` GiB. Use `0` to only move the largest single project on a shard.
* `MOVE_LIMIT`: Default is `-1` (no limit). This is the maximum amount of migrations. 

## Setup

Install asdf:

```bash
source ~/.github
export asdf_release=$(curl --insecure --location --silent --show-error --header "Authorization: token ${GITLAB_PRODUCTION_OATH_PERSONAL_API_TOKEN}" --header "Accept: application/vnd.github.v3+json" "https://api.github.com/repos/asdf-vm/asdf/tags" | jq -r '.[0]["name"]')
export asdf_release="${asdf_release:-v0.8.1}"
git clone https://github.com/asdf-vm/asdf.git "${HOME}/.asdf" --branch "${asdf_release}"
pushd "${HOME}/.asdf"; git fetch origin; popd
source "${HOME}/.asdf/asdf.sh"; source "${HOME}/.asdf/completions/asdf.bash"
```

Install ruby:

```bash
asdf plugin add ruby
asdf install ruby
```

Install the project dependencies:

```bash
gem install bundler
bundle install
```


## Invocations


### Example shard selection

Note that to reach thanos from outside the network, one must establish a
tunnel through the `staging` bastion in another terminal window:

```sh
ssh -L 10902:thanos-query-frontend-internal.ops.gke.gitlab.net:9090 lb-bastion.gstg.gitlab.com
```

Then run the shard selection program specifying the development environment
argument.

```sh
rm -f ./shards.json
bundle exec lib/shards.rb --environment=development
cat shards.json | jq
```

```json
{
  "shards": [
    "nfs-file01"
  ]
}
```


### Example project selection

```bash
export GITLAB_GSTG_ADMIN_API_PRIVATE_TOKEN='changeme'
rm -f ./projects.json
bundle exec lib/projects.rb --shards=./shards.json --output=./projects.json --limit=2
cat projects.json | jq
```

```json
{
  "projects": [
    {
      "id": 1,
      "path_with_namespace": "namespace/project_name_01",
      "human_readable_size": "1.72 GB",
      "repository_storage": "nfs-file01"
    },
    {
      "id": 2,
      "path_with_namespace": "namespace/project_name_02",
      "human_readable_size": "1.67 GB",
      "repository_storage": "nfs-file01"
    }
  ]
}
```


### Example project move

```bash
bundle exec lib/mover.rb --projects=./projects.json --limit=1 --dry-run=yes
2020-11-11 00:05:55 INFO  [Dry-run] This is only a dry-run -- write operations will be logged but not executed
2020-11-11 00:05:55 INFO  Will move 1 projects
2020-11-11 00:05:57 INFO  ======================================================
2020-11-11 00:05:57 INFO  Scheduling repository move for project id: 1
2020-11-11 00:05:57 INFO    Project path: namespace/project_name_01
2020-11-11 00:05:57 INFO    Current shard name: nfs-file01
2020-11-11 00:05:57 INFO    Repository size: 1.72 GB
2020-11-11 00:05:57 INFO  [Dry-run] Would have scheduled repository move for project id: 1
2020-11-11 00:05:57 INFO  ======================================================
2020-11-11 00:05:57 INFO  Done
2020-11-11 00:05:57 INFO  [Dry-run] Would have processed 1.72 GB of data
```


## Pipeline stages

`TODO`


## Schedule

For the `.com` environment:

```bash
rm -f ./pipeline.json
export ops_project_id=419
export gitlab_com_project_id=22418805
# Create pipeline schedule:
curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form description="Balance shards [staging]" --form ref="main" --form cron="0 1 * * *" --form cron_timezone="UTC" --form active="true" "https://gitlab.com/api/v4/projects/${gitlab_com_project_id}/pipeline_schedules" > ./pipeline.json
export pipeline_id=$(cat ./pipeline.json | jq -r '.["id"]')
# Create pipeline schedule variables:
curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form "key=SCHEDULED_EXECUTION" --form "value=true" "https://gitlab.com/api/v4/projects/${gitlab_com_project_id}/pipeline_schedules/${pipeline_id}/variables" | jq
```

For the `gprd` environment:

```bash
rm -f ./pipeline.json
export gitlab_com_project_id=22418805
# Create pipeline schedule:
curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form description="Balance shards [staging]" --form ref="main" --form cron="0 1 * * *" --form cron_timezone="UTC" --form active="true" "https://gitlab.com/api/v4/projects/${gitlab_com_project_id}/pipeline_schedules" > ./pipeline.json
export pipeline_id=$(cat ./pipeline.json | jq -r '.["id"]')
# Create pipeline schedule variables:
curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" --form "key=SCHEDULED_EXECUTION" --form "value=true" "https://gitlab.com/api/v4/projects/${gitlab_com_project_id}/pipeline_schedules/${pipeline_id}/variables" | jq
```

For the `ops` environment:

```bash
rm -f ./pipeline.json
export ops_project_id=419
# Create pipeline schedule:
curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_OPS_API_PRIVATE_TOKEN}" --form description="Balance shards [staging]" --form ref="main" --form cron="0 1 * * *" --form cron_timezone="UTC" --form active="true" "https://ops.gitlab.net/api/v4/projects/${ops_project_id}/pipeline_schedules" > ./pipeline.json
export pipeline_id=$(cat ./pipeline.json | jq -r '.["id"]')
# Create pipeline schedule variables:
curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_OPS_API_PRIVATE_TOKEN}" --form "key=SCHEDULED_EXECUTION" --form "value=true" "https://gitlab.com/api/v4/projects/${ops_project_id}/pipeline_schedules/${pipeline_id}/variables" | jq
```


## Create an MR and merge it

```bash
export branch_name=$(git branch --show)
export gitlab_com_project_id=22418805
git push origin "${branch_name}"
export merge_request_id=$(curl --silent --show-error --location --request POST --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${gitlab_com_project_id}/merge_requests" --form "source_branch=${branch_name}" --form "target_branch=main" --form "title=${branch_name}" --form "remove_source_branch=true" | jq .iid)
echo "Merge request: ${merge_request_id}"
export merge_status=
until [ "${merge_status}" == 'can_be_merged' ]; do [[ -z "${merge_status}" ]] || sleep 2; export merge_status=$(curl --silent --show-error --location --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${gitlab_com_project_id}/merge_requests/${merge_request_id}" | jq -r .merge_status); echo "Merge request status: ${merge_status}"; done
curl --silent --show-error --location --request PUT --header "PRIVATE-TOKEN: ${GITLAB_COM_API_PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${gitlab_com_project_id}/merge_requests/${merge_request_id}/merge"
git fetch origin; git checkout main; git pull origin main; git branch -D "${branch_name}"
```


## Delete an MR

```bash
export ops_project_id=419
curl --silent --show-error --location --request DELETE --header "PRIVATE-TOKEN: ${GITLAB_OPS_API_PRIVATE_TOKEN}" "https://ops.gitlab.net/api/v4/projects/${ops_project_id}/merge_requests/${merge_request_id}"
```


## Force a mirror sync

```bash
curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_OPS_API_PRIVATE_TOKEN}" "https://ops.gitlab.net/api/v4/projects/${ops_project_id}/mirror/pull" && echo
```


## Manifest

```bash
$ tree -I .git -I vendor
```

```
.
├── Gemfile
├── Gemfile.lock
├── README.md
├── lib
│   ├── gitlab
│   │   └── client.rb
│   ├── helpers
│   │   ├── debugging.rb
│   │   ├── filesize.rb
│   │   └── logging.rb
│   ├── mover.rb
│   ├── projects.rb
│   ├── shards.rb
│   └── thanos
│       └── client.rb
├── log
├── shards.promql
├── spec
│   ├── mover_spec.rb
│   └── spec_helper.rb
└── storage_migrations

3 directories, 7 files
```
