# Based on https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/job_completion.md#creating-and-updating-a-new-job-metric
# This script reports success to the pushgateway

set -eufo pipefail

cat <<PROM | curl -iv --data-binary @- "http://${PUSH_GATEWAY}:9091/metrics/job/${JOB}/tier/${TIER}/type/${TYPE}"
# HELP gitlab_job_failed Boolean status of the job.
# TYPE gitlab_job_failed gauge
gitlab_job_failed{resource="${RESOURCE}"} 1
PROM